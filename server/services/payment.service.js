const config = require('../config.json');
const _ = require('lodash');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const Q = require('q');
const mysql = require('mysql');
const connection = mysql.createConnection({
  host: 'utruckingdb.c23ekfuplaqh.us-east-1.rds.amazonaws.com',
  user: 'owner1986',
  password: 'pacvan34',
  database: 'utrucking'
});
var ApiContracts = require('authorizenet').APIContracts;
var ApiControllers = require('authorizenet').APIControllers;
var SDKConstants = require('authorizenet').Constants;


var service = {};


service.charge = charge;
service.getByID = getByID;

function getByID(id) {
  var deferred = Q.defer();
  connection.query('SELECT * FROM `Payment` WHERE student_id=?', id, function (error, payments, fields) {
    if (error){
        deferred.reject(error.name + ': ' + error.message)
    }
    deferred.resolve(payments);
  });
  return deferred.promise;
}

function charge(card) {
  var deferred = Q.defer();
  chargeCard(card).then(function (response){
    connection.query('INSERT INTO `Payment` (student_id, date, amount, note, auth_code, transaction_num) VALUES (?,?,?,?,?,?)', [card.student_id, new Date().toLocaleString(), card.amount, card.note, response.auth_id, response.transaction_num] , function (error, results, fields) {
      if (error){
          deferred.reject(error.name + ': ' + error.message)
      }
      deferred.resolve();
    });
  }).catch(function(err){
    deferred.reject(err);
  });
  return deferred.promise;
}

function chargeCard(card) {
  var deferred = Q.defer();
  var merchantAuthenticationType = new ApiContracts.MerchantAuthenticationType();
	merchantAuthenticationType.setName('8QPau9P63');
	merchantAuthenticationType.setTransactionKey('4y2yS44Z3GQ2xr94');

	var creditCard = new ApiContracts.CreditCardType();
	creditCard.setCardNumber(card.cc_number);
	creditCard.setExpirationDate(card.cc_expiration);
	creditCard.setCardCode(card.cc_code);

  var orderDetails = new ApiContracts.OrderType();
	orderDetails.setDescription(card.note);

	var paymentType = new ApiContracts.PaymentType();
	paymentType.setCreditCard(creditCard);

	var billTo = new ApiContracts.CustomerAddressType();
	billTo.setFirstName(card.first_name);
	billTo.setLastName(card.last_name);
	billTo.setAddress(card.street);
	billTo.setCity(card.city);
	billTo.setState(card.state);
	billTo.setZip(card.zip);
	billTo.setCountry(card.country);

	var transactionRequestType = new ApiContracts.TransactionRequestType();
	transactionRequestType.setTransactionType(ApiContracts.TransactionTypeEnum.AUTHCAPTURETRANSACTION);
	transactionRequestType.setPayment(paymentType);
	transactionRequestType.setAmount(card.amount);
	transactionRequestType.setBillTo(billTo);
  transactionRequestType.setOrder(orderDetails);

	var createRequest = new ApiContracts.CreateTransactionRequest();
	createRequest.setMerchantAuthentication(merchantAuthenticationType);
	createRequest.setTransactionRequest(transactionRequestType);

	//pretty print request
	console.log(JSON.stringify(createRequest.getJSON(), null, 2));

	var ctrl = new ApiControllers.CreateTransactionController(createRequest.getJSON());
	//Defaults to sandbox
	//ctrl.setEnvironment(SDKConstants.endpoint.production);

	ctrl.execute(function(){
    var authID;
    var transID;
		var apiResponse = ctrl.getResponse();

		var response = new ApiContracts.CreateTransactionResponse(apiResponse);

		if(response != null){
			if(response.getMessages().getResultCode() == ApiContracts.MessageTypeEnum.OK){
				if(response.getTransactionResponse().getMessages() != null){
					console.log('Successfully created transaction with Transaction ID: ' + response.getTransactionResponse().getTransId());
          transID = response.getTransactionResponse().getTransId();
          authID = response.getTransactionResponse().getAuthCode();
					console.log('Description: ' + response.getTransactionResponse().getMessages().getMessage()[0].getDescription());
				}
				else {
					if(response.getTransactionResponse().getErrors() != null){
            deferred.reject(response.getTransactionResponse().getErrors().getError()[0].getErrorCode() + ': ' + response.getTransactionResponse().getErrors().getError()[0].getErrorText());
					}
				}
			}
			else {
				if(response.getTransactionResponse() != null && response.getTransactionResponse().getErrors() != null){
          deferred.reject(response.getTransactionResponse().getErrors().getError()[0].getErrorCode() + ': ' + response.getTransactionResponse().getErrors().getError()[0].getErrorText());

				}
				else {
          deferred.reject(response.getMessages().getMessage()[0].getCode() + ': ' + response.getMessages().getMessage()[0].getText());
				}
			}
		}
		else {
      deferred.reject("Null Response");
		}
    if((authID != "") && (transID != "")){
      console.log(authID);
      console.log(transID);
      deferred.resolve({
        auth_id: authID,
        transaction_num: transID
      });
    }
	});
    return deferred.promise;
}
module.exports = service;
