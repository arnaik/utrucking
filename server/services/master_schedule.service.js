const config = require('../config.json');
const _ = require('lodash');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const Q = require('q');
const mysql = require('mysql');
const connection = mysql.createConnection({
  host: 'utruckingdb.c23ekfuplaqh.us-east-1.rds.amazonaws.com',
  user: 'owner1986',
  password: 'pacvan34',
  database: 'utrucking'
});



var service = {};

service.getByID = getByID;
service.getOptions = getOptions;
service.getMax = getMax;


function getByID(studentID) {
  var deferred = Q.defer();
  connection.query('SELECT MasterSchedule.master_schedule_id, MasterSchedule.date,MasterSchedule.start_time,MasterSchedule.end_time FROM `MasterSchedule` JOIN `Schedule` ON MasterSchedule.master_schedule_id=Schedule.master_schedule_id WHERE Schedule.student_id=?', studentID, function (error, master, fields) {
    if (error){
        deferred.reject(error.name + ': ' + error.message)
    }
    console.log(master);
    if(master.length == 0){
      deferred.resolve({
        result: "EOF"
      });
    }
    else{
      deferred.resolve({
        result: master[0]
      });
    }
  });
  return deferred.promise;
}
function getMax(id) {
  var deferred = Q.defer();
  connection.query('SELECT max_number FROM `MasterSchedule` WHERE master_schedule_id=?', id, function (error, max, fields) {
    if (error){
        deferred.reject(error.name + ': ' + error.message)
    }
    if(max.length == 0){
      deferred.resolve({
        result: "EOF"
      });
    }
    else{
      deferred.resolve({
        result: max[0]
      });
    }
  });
  return deferred.promise;
}
function getOptions(params) {
  var deferred = Q.defer();
  connection.query('SELECT MasterSchedule.master_schedule_id,MasterSchedule.date,MasterSchedule.start_time,MasterSchedule.end_time FROM `MasterSchedule` JOIN `LocationLink` ON MasterSchedule.truck_location_id=LocationLink.truck_location_id JOIN `Student` ON LocationLink.living_location_id=Student.living_loc_id WHERE Student.student_id=? AND STR_TO_DATE(MasterSchedule.date, "%m/%d/%Y") >= ? AND STR_TO_DATE(MasterSchedule.date, "%m/%d/%Y") <= ? ORDER BY MasterSchedule.date ASC,MasterSchedule.start_time ASC', [params.id, params.start_date, params.end_date], function(error, options, fields) {
    if (error){
        deferred.reject(error.name + ': ' + error.message)
    }
    if(options.length == 0){
      deferred.resolve({
        result: "EOF"
      });
    }
    else{
      deferred.resolve({
        result: options
      });
    }
  });
  return deferred.promise;
}
module.exports = service;
