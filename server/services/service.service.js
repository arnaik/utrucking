const config = require('../config.json');
const _ = require('lodash');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const Q = require('q');
const mysql = require('mysql');
const connection = mysql.createConnection({
  host: 'utruckingdb.c23ekfuplaqh.us-east-1.rds.amazonaws.com',
  user: 'owner1986',
  password: 'pacvan34',
  database: 'utrucking'
});



var service = {};

service.getAvailable = getAvailable;
service.create = create;
service.getServiceByID = getServiceByID;
service.getServiceDates = getServiceDates;

function getAvailable() {
  var deferred = Q.defer();
  connection.query('SELECT service_id,name,page,signup_end_date,hidden,total,require_schedule FROM `Service` WHERE STR_TO_DATE(signup_start_date, "%m/%d/%Y")<=? AND STR_TO_DATE(signup_end_date, "%m/%d/%Y")>=?', [(new Date()).toISOString().substring(0, 10),(new Date()).toISOString().substring(0, 10)], function (error, services, fields) {
    if (error){
        deferred.reject(error.name + ': ' + error.message)
    }
    deferred.resolve(services);
  });
  return deferred.promise;
}

function getServiceByID(id) {
  var deferred = Q.defer();
  connection.query('SELECT * FROM `Service` WHERE service_id=?', id, function (error, service, fields) {
    if (error){
        deferred.reject(error.name + ': ' + error.message)
    }
    deferred.resolve(service[0]);
  });
  return deferred.promise;
}
function getServiceDates(id) {
  var deferred = Q.defer();
  connection.query('SELECT Service.start_date,Service.end_date FROM `Service` JOIN `ServiceLink` ON Service.service_id=ServiceLink.service_id JOIN `Student` ON ServiceLink.student_id=Student.student_id WHERE Service.require_schedule = 1 AND Student.student_id=? ORDER BY Service.start_date LIMIT 1', id, function (error, serviceDates, fields) {
    if (error){
        deferred.reject(error.name + ': ' + error.message)
    }
    if(serviceDates.length == 0){
      deferred.resolve({
        result: "EOF"
      });
    }
    else{
      deferred.resolve({
        result: serviceDates[0]
      });
    }
  });
  return deferred.promise;
}
function create() {
  // var deferred = Q.defer();
  // connection.query('SELECT * FROM `Service`', function (error, services, fields) {
  //   if (error){
  //       deferred.reject(error.name + ': ' + error.message)
  //   }
  //   deferred.resolve(services);
  // });
  // return deferred.promise;
}
module.exports = service;
