const config = require('../config.json');
const _ = require('lodash');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const Q = require('q');
const mysql = require('mysql');
const connection = mysql.createConnection({
  host: 'utruckingdb.c23ekfuplaqh.us-east-1.rds.amazonaws.com',
  user: 'owner1986',
  password: 'pacvan34',
  database: 'utrucking'
});



var service = {};

service.create = create;
service.getLink = getLink;
service.getCount = getCount;

function create(params) {
  var deferred = Q.defer();
  var serviceID = params.serviceID;
  var userID = params.userID;
  connection.query('INSERT INTO `ServiceLink` (service_id, student_id, date) VALUES (?,?,?)', [serviceID, userID, new Date().toLocaleDateString()] , function (error, results, fields) {
    if (error){
        deferred.reject(error.name + ': ' + error.message)
    }
    deferred.resolve();
  });
  return deferred.promise;
}
function getLink(serviceLink) {
  var deferred = Q.defer();
  connection.query('SELECT * FROM `ServiceLink` WHERE service_id=? AND student_id=?', [parseInt(serviceLink.service_id), parseInt(serviceLink.user_id)] , function (error, link, fields) {
    if (error){
        deferred.reject(error.name + ': ' + error.message)
    }
    if(link.length == 0){
      deferred.resolve({
        result: "EOF"
      });
    }
    else{
      deferred.resolve({
        result: "ALREADY_EXISTS"
      });
    }
  });
  return deferred.promise;
}
function getCount(id) {
  var deferred = Q.defer();
  connection.query('SELECT COUNT(student_id) AS count FROM `ServiceLink` WHERE service_id=?', id, function (error, count, fields) {
    if (error){
        deferred.reject(error.name + ': ' + error.message)
    }
    deferred.resolve(count);
  });
  return deferred.promise;
}
module.exports = service;
