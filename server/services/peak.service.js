const config = require('../config.json');
const _ = require('lodash');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const Q = require('q');
const mysql = require('mysql');
const connection = mysql.createConnection({
  host: 'utruckingdb.c23ekfuplaqh.us-east-1.rds.amazonaws.com',
  user: 'owner1986',
  password: 'pacvan34',
  database: 'utrucking'
});



var service = {};

service.getPeak = getPeak;

function getPeak(date) {
  var deferred = Q.defer();
  connection.query('SELECT price FROM `Peak` WHERE STR_TO_DATE(date,"%m/%d/%Y")=? ORDER BY price LIMIT 1', date, function (error, peak, fields) {
    if (error){
        deferred.reject(error.name + ': ' + error.message)
    }
    if(peak.length == 0){
      deferred.resolve({
        result: "EOF"
      });
    }
    else{
      deferred.resolve({
        result: peak[0]
      });
    }
  });
  return deferred.promise;
}
module.exports = service;
