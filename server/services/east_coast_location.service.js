const config = require('../config.json');
const _ = require('lodash');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const Q = require('q');
const mysql = require('mysql');
const connection = mysql.createConnection({
  host: 'utruckingdb.c23ekfuplaqh.us-east-1.rds.amazonaws.com',
  user: 'owner1986',
  password: 'pacvan34',
  database: 'utrucking'
});
var service = {};

service.getAll = getAll;
service.getByID = getByID;


function getAll() {
  var deferred = Q.defer();
  connection.query('SELECT * FROM `EastCoastLocation`', function (error, locations, fields) {
    if (error){
        deferred.reject(error.name + ': ' + error.message)
    }
    console.log("in east coast service");
    console.log(locations);
    deferred.resolve(locations);
  });
  return deferred.promise;
}

function getByID(id) {
  var deferred = Q.defer();
  connection.query('SELECT EastCoastLocation.* FROM `EastCoastLocation` JOIN `Student` ON EastCoastLocation.east_coast_location_id=Student.east_coast_location_id WHERE Student.student_id=?', id, function (error, location, fields) {
    if (error){
        deferred.reject(error.name + ': ' + error.message)
    }
    if(location.length == 0){
      deferred.resolve({
        result: "EOF"
      });
    }
    else{
      deferred.resolve({
        result: location[0]
      });
    }
  });
  return deferred.promise;
}
module.exports = service;
