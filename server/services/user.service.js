const config = require('../config.json');
const _ = require('lodash');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const Q = require('q');
const mongo = require('mongoskin');
const MongoClient = require('mongodb').MongoClient;
const mysql = require('mysql');
const connection = mysql.createConnection({
  host: 'utruckingdb.c23ekfuplaqh.us-east-1.rds.amazonaws.com',
  user: 'owner1986',
  password: 'pacvan34',
  database: 'utrucking'
});



var service = {};

service.getAll = getAll;
service.getByID = getByID;
service.create = create;
service.authenticate = authenticate;
service.updateOffCampusInfo = updateOffCampusInfo;
service.updateOnCampusInfo = updateOnCampusInfo;
service.updateBalance = updateBalance;
service.updateScheduling = updateScheduling;
service.updatePassword = updatePassword;
service.updateAccount = updateAccount;
service.updatePickup = updatePickup;
service.updateBilling = updateBilling;

function authenticate(email, password) {
    var deferred = Q.defer();
    connection.query('SELECT * FROM `Student` WHERE student_email=?', email,
      function (error, student, fields) {
        if (error){
          deferred.reject(error.name + ': ' + error.message)
        }
        if (student && (password == student[0].password)){
          deferred.resolve({
              id: student[0].student_id,
              email: student[0].student_email,
              first: student[0].first_name,
              last: student[0].last_name,
              token: jwt.sign({ sub: student[0].student_id }, config.secret)
          });
        } else {
          deferred.resolve();
        }
    });
    return deferred.promise;
}
function getAll() {
  var deferred = Q.defer();
  connection.query('SELECT * FROM `Student`', function (error, students, fields) {
    if (error){
        deferred.reject(error.name + ': ' + error.message)
    }
    deferred.resolve(students);
  });
  return deferred.promise;
}
function getByID(id) {
  var deferred = Q.defer();
  connection.query('SELECT * FROM `Student` WHERE student_id=?', id, function (error, student, fields) {
    if (error){
        deferred.reject(error.name + ': ' + error.message)
    }
    deferred.resolve(student);
  });
  return deferred.promise;
}
function create(user) {
    var deferred = Q.defer();
    connection.query('SELECT * FROM `Student` WHERE student_email=?', user.email,
      function (error, student, fields) {
        if (error){
          deferred.reject(error.name + ': ' + error.message)
        }
        if (student.length != 0){
          deferred.reject('This account already exists');
        }
        else{
          createUser();
      }
    });
  function createUser() {
    // set user object to userParam without the cleartext password
    var addrID;
    var newUser = _.omit(user, 'password');
    // add hashed password to user object
    newUser.password = user.password;
    connection.query('INSERT INTO `Student` (first_name,last_name,student_email,password,home_addr_id,home_phone,cell_phone,grad_year,signup_date,parent_email) VALUES (?,?,?,?,?,?,?,?,?,?)', [newUser.first_name, newUser.last_name, newUser.email,newUser.password,newUser.home_addr_id,newUser.home_phone,newUser.cell_phone,newUser.grad_year,new Date().toLocaleString(),newUser.parent_email],
      function (error) {
        if (error){
          deferred.reject(error.name + ': ' + error.message)
        }
        deferred.resolve();
        });
    }
    return deferred.promise;
}
function updateOffCampusInfo(params) {
  var deferred = Q.defer();
  connection.query('UPDATE `Student` SET living_loc_id=?, local_addr_id=?, room=? WHERE student_id=?', [params.livingLocationID, params.localAddressID, 0, params.userID], function (error, students, fields) {
    if (error){
        deferred.reject(error.name + ': ' + error.message)
    }
    deferred.resolve();
  });
  return deferred.promise;
}
function updateOnCampusInfo(params) {
  var deferred = Q.defer();
  console.log(params.room);
  connection.query('UPDATE `Student` SET living_loc_id=?, local_addr_id=?, room=? WHERE student_id=?', [params.livingLocationID, 0, params.room,params.userID], function (error, students, fields) {
    if (error){
        deferred.reject(error.name + ': ' + error.message)
    }
    deferred.resolve();
  });
  return deferred.promise;
}
function updateBalance(student) {
  console.log("updating!");
  var deferred = Q.defer();
  connection.query('UPDATE `Student` SET balance=?, balance_text=? WHERE student_id=?', [student.balance, student.balance_text, student.student_id], function (error, results, fields) {
    if (error){
        deferred.reject(error.name + ': ' + error.message)
    }
    deferred.resolve();
  });
  return deferred.promise;
}
function updateScheduling(params) {
  var deferred = Q.defer();
  connection.query('UPDATE `Student` SET balance=?, balance_text=?, sched_price=? WHERE student_id=?', [params.balance, params.balance_text, params.sched_price, params.student_id], function (error, results, fields) {
    if (error){
        deferred.reject(error.name + ': ' + error.message)
    }
    console.log('all good?');
    deferred.resolve();
  });
  return deferred.promise;
}
function updateAccount(student) {
  var deferred = Q.defer();
  connection.query('UPDATE `Student` SET first_name=?, last_name=?, student_email=?, home_phone=?, local_phone=?, cell_phone=?, grad_year=? WHERE student_id=?', [student.first_name, student.last_name, student.student_email, student.home_phone, student.cell_phone, student.local_phone, student.grad_year, student.student_id], function (error, students, fields) {
    if (error){
        deferred.reject(error.name + ': ' + error.message)
    }
    console.log("done!");
    deferred.resolve();
  });
  return deferred.promise;
}
function updatePassword(student) {
  var deferred = Q.defer();
  connection.query('UPDATE `Student` SET password=? WHERE student_id=?', [student.password, student.student_id], function (error, students, fields) {
    if (error){
        deferred.reject(error.name + ': ' + error.message)
    }
    console.log("done!");
    deferred.resolve();
  });
  return deferred.promise;
}
function updatePickup(student) {
  var deferred = Q.defer();
  connection.query('UPDATE `Student` SET east_coast_location_id=?, ship_to_utrucking=? WHERE student_id=?', [student.east_coast_location_id, student.ship_to_utrucking, student.student_id], function (error, students, fields) {
    if (error){
        deferred.reject(error.name + ': ' + error.message)
    }
    console.log("done!");
    deferred.resolve();
  });
  return deferred.promise;
}
function updateBilling(student) {
  var deferred = Q.defer();
  connection.query('UPDATE `Student` SET billing_addr_id=? WHERE student_id=?', [student.billing_addr_id, student.student_id], function (error, students, fields) {
    if (error){
        deferred.reject(error.name + ': ' + error.message)
    }
    console.log("done!");
    deferred.resolve();
  });
  return deferred.promise;
}
module.exports = service;
