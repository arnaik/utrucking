const config = require('../config.json');
const _ = require('lodash');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const Q = require('q');
const mysql = require('mysql');
const connection = mysql.createConnection({
  host: 'utruckingdb.c23ekfuplaqh.us-east-1.rds.amazonaws.com',
  user: 'owner1986',
  password: 'pacvan34',
  database: 'utrucking'
});



var service = {};

service.create = create;
service.getHomeAddress = getHomeAddress;
service.getShippingAddress = getShippingAddress;
service.getLocalAddress = getLocalAddress;
service.getBillingAddress = getBillingAddress;
service.updateAddress = updateAddress;

function getHomeAddress(id) {
  var deferred = Q.defer();
  connection.query('SELECT (home_addr_id) FROM `Student` WHERE student_id=?', id, function (error, homeAddressID, fields) {
    if (error){
        deferred.reject(error.name + ': ' + error.message)
    }
    connection.query('SELECT * FROM `Address` WHERE address_id=?', homeAddressID[0].home_addr_id, function (error, homeAddress, fields) {
      if (error){
          deferred.reject(error.name + ': ' + error.message)
      }
      deferred.resolve(homeAddress[0]);
    });
  });
  return deferred.promise;
}
function getBillingAddress(id) {
  var deferred = Q.defer();
  connection.query('SELECT (billing_addr_id) FROM `Student` WHERE student_id=?', id, function (error, billingAddressID, fields) {
    if (error){
        deferred.reject(error.name + ': ' + error.message)
    }
    if(billingAddressID[0].billing_addr_id){
      connection.query('SELECT * FROM `Address` WHERE address_id=?', billingAddressID[0].billing_addr_id, function (error, billingAddress, fields) {
        if (error){
            deferred.reject(error.name + ': ' + error.message)
        }
        deferred.resolve(billingAddress[0]);
      });
    }else{
      deferred.resolve({
        address_id: -1
      });
    }
      });
  return deferred.promise;
}
function getShippingAddress(id) {
  var deferred = Q.defer();
  connection.query('SELECT Address.* FROM `Address` JOIN `Schedule` ON Address.address_id=Schedule.shipping_addr_id WHERE Schedule.student_id=?', id, function (error, shippingAddress, fields) {
    if (error){
        deferred.reject(error.name + ': ' + error.message)
    }
    if(shippingAddress.length == 0){
      deferred.resolve({
        result: "EOF"
      });
    }
    else{
      deferred.resolve({
        result: shippingAddress[0]
      });
    }
  });
  return deferred.promise;
}
function getLocalAddress(id) {
  var deferred = Q.defer();
  connection.query('SELECT Address.* FROM `Address` JOIN `Student` ON Address.address_id=Student.local_addr_id WHERE Student.student_id=?', id, function (error, localAddress, fields) {
    if (error){
        deferred.reject(error.name + ': ' + error.message)
    }
    if(localAddress.length == 0){
      deferred.resolve({
        result: "EOF"
      });
    }
    else{
      deferred.resolve({
        result: localAddress[0]
      });
    }
  });
  return deferred.promise;
}
function create(newAddress) {
  var deferred = Q.defer();
  connection.query('INSERT INTO `Address` (street, city, state, country, zip, province) VALUES (?,?,?,?,?,?)', [newAddress.street, newAddress.city, newAddress.state, newAddress.country, newAddress.zip, newAddress.province], function (error, results, fields) {
    if (error){
        deferred.reject(error.name + ': ' + error.message)
    }
    deferred.resolve(
      {
        insertID: results.insertId
      });
  });
  return deferred.promise;
}
function updateAddress(newAddress) {
  var deferred = Q.defer();
  connection.query('UPDATE `Address` SET street=?,city=?,state=?,country=?,zip=?,province=? WHERE address_id=?', [newAddress.street, newAddress.city, newAddress.state, newAddress.country, newAddress.zip, newAddress.province, newAddress.address_id], function (error, results, fields) {
    if (error){
        deferred.reject(error.name + ': ' + error.message)
    }
    deferred.resolve();
  });
  return deferred.promise;
}
module.exports = service;
