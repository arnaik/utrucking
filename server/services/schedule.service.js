const config = require('../config.json');
const _ = require('lodash');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const Q = require('q');
const mysql = require('mysql');
const connection = mysql.createConnection({
  host: 'utruckingdb.c23ekfuplaqh.us-east-1.rds.amazonaws.com',
  user: 'owner1986',
  password: 'pacvan34',
  database: 'utrucking'
});



var service = {};

service.create = create;
service.update = update;
service.updateMaster = updateMaster;
service.getShipID = getShipID;
service.getUserID = getUserID;
service.getCountPerTimeSlot = getCountPerTimeSlot;
service.getCountPerDay = getCountPerDay;

function create(schedule) {
  var deferred = Q.defer();
  connection.query('INSERT INTO `Schedule` (student_id, master_schedule_id, shipping_addr_id) VALUES (?,?,?)', [schedule.student_id, schedule.master_schedule_id, schedule.shipping_addr_id] , function (error, results, fields) {
    if (error){
        deferred.reject(error.name + ': ' + error.message)
    }
    deferred.resolve();
  });
  return deferred.promise;
}
function update(schedule) {
  var deferred = Q.defer();
  connection.query('UPDATE `Schedule` SET shipping_addr_id=? WHERE student_id=?', [schedule.shipping_addr_id, schedule.student_id] , function (error, results, fields) {
    if (error){
        deferred.reject(error.name + ': ' + error.message)
    }
    deferred.resolve();
  });
  return deferred.promise;
}
function updateMaster(params) {
  var deferred = Q.defer();
  connection.query('UPDATE `Schedule` SET master_schedule_id=? WHERE student_id=?', [params.master_schedule_id, params.student_id] , function (error, results, fields) {
    if (error){
        deferred.reject(error.name + ': ' + error.message)
    }
    deferred.resolve();
  });
  return deferred.promise;
}
function getShipID(userID) {
  var deferred = Q.defer();
  connection.query('SELECT (shipping_addr_id) FROM `Schedule` WHERE student_id=?', userID, function (error, shippingAddrID, fields) {
    if (error){
        deferred.reject(error.name + ': ' + error.message)
    }
    if(shippingAddrID.length == 0){
      deferred.resolve({
        result: "EOF"
      });
    }
    else if(shippingAddrID[0].shipping_addr_id){
      deferred.resolve({
        result: shippingAddrID[0].shipping_addr_id
      });
    }
  });
  return deferred.promise;
}
function getUserID(userID) {
  var deferred = Q.defer();
  connection.query('SELECT (student_id) FROM `Schedule` WHERE student_id=?', userID, function (error, userID, fields) {
    if (error){
        deferred.reject(error.name + ': ' + error.message)
    }
    if(userID.length == 0){
      deferred.resolve({
        result: "EOF"
      });
    }
    else {
      deferred.resolve({
        result: userID[0].student_id
      });
    }
  });
  return deferred.promise;
}
function getCountPerTimeSlot(id) {
  var deferred = Q.defer();
  connection.query('SELECT COUNT(schedule_id) AS count FROM `Schedule` WHERE master_schedule_id=?', id, function (error, count, fields) {
    if (error){
        deferred.reject(error.name + ': ' + error.message)
    }
    if(count.length == 0){
      deferred.resolve({
        result: "EOF"
      });
    }
    else {
      deferred.resolve({
        result: count[0]
      });
    }
  });
  return deferred.promise;
}
function getCountPerDay(date) {
  var deferred = Q.defer();
  connection.query('SELECT COUNT(*) AS count FROM `Schedule` JOIN `MasterSchedule` ON MasterSchedule.master_schedule_id=Schedule.master_schedule_id WHERE MasterSchedule.date=?', date, function (error, count, fields) {
    if (error){
        deferred.reject(error.name + ': ' + error.message)
    }
    deferred.resolve(count[0]);
  });
  return deferred.promise;
}
module.exports = service;
