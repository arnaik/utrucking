const config = require('../config.json');
const _ = require('lodash');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const Q = require('q');
const mysql = require('mysql');
const connection = mysql.createConnection({
  host: 'utruckingdb.c23ekfuplaqh.us-east-1.rds.amazonaws.com',
  user: 'owner1986',
  password: 'pacvan34',
  database: 'utrucking'
});



var service = {};

service.getAll = getAll;
service.getLocation = getLocation;

function getAll() {
  var deferred = Q.defer();
  connection.query('SELECT * FROM `LivingLocation`', function (error, livingLocations, fields) {
    if (error){
        deferred.reject(error.name + ': ' + error.message)
    }
    deferred.resolve(livingLocations);
  });
  return deferred.promise;
}
function getLocation(id) {
  var deferred = Q.defer();
  connection.query('SELECT LivingLocation.* FROM `LivingLocation` JOIN `Student` ON LivingLocation.living_location_id=Student.living_loc_id WHERE Student.student_id=?', id, function (error, livingLocation, fields) {
    if (error){
        deferred.reject(error.name + ': ' + error.message)
    }
    if(livingLocation.length == 0){
      deferred.resolve({
        result: "EOF"
      });
    }
    else{
      deferred.resolve({
        result: livingLocation[0]
      });
    }
  });
  return deferred.promise;
}
module.exports = service;
