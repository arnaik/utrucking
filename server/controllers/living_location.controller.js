var config = require('../config.json');
var express = require('express');
var router = express.Router();
var assert = require('assert');
var livingLocationService = require('../services/living_location.service');
const app = express();
const expressJwt = require('express-jwt');

router.get('/', getAll);
router.get('/get_location/:id', getLocation);

router.use(expressJwt({
    secret: config.secret,
    getToken: function (req) {
        if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
            return req.headers.authorization.split(' ')[1];
        } else if (req.query && req.query.token) {
            return req.query.token;
        }
        return null;
    }
}));

function getAll(req, res) {

    livingLocationService.getAll().then(function (livingLocations) {
            res.send(livingLocations);
        }).catch(function (err) {
            res.send(400).send(err);
        });
}
function getLocation(req, res) {
    livingLocationService.getLocation(req.params.id).then(function (livingLocation) {
            res.send(livingLocation);
        }).catch(function (err) {
            res.send(400).send(err);
        });
}
module.exports = router;
