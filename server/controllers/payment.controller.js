var config = require('../config.json');
var express = require('express');
var router = express.Router();
var assert = require('assert');
var paymentService = require('../services/payment.service');
const app = express();
const expressJwt = require('express-jwt');

router.post('/charge', chargeCard);
router.get('/:id', getByID);

router.use(expressJwt({
    secret: config.secret,
    getToken: function (req) {
        if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
            return req.headers.authorization.split(' ')[1];
        } else if (req.query && req.query.token) {
            return req.query.token;
        }
        return null;
    }
}));

function chargeCard(req, res) {
    paymentService.charge(req.body).then(function () {
            res.send(200);
        }).catch(function (err) {
            res.status(400).send(err);
        });
}
function getByID(req, res) {
    paymentService.getByID(req.params.id).then(function (payments) {
            res.send(payments);
        }).catch(function (err) {
            res.status(400).send(err);
        });
}
module.exports = router;
