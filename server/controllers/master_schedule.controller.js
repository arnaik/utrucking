var config = require('../config.json');
var express = require('express');
var router = express.Router();
var assert = require('assert');
var masterScheduleService = require('../services/master_schedule.service');
const app = express();
const expressJwt = require('express-jwt');

router.get('/get_master/:id', getByID);
router.get('/get_options/:id&:start_date&:end_date', getOptions);
router.get('/get_max/:id', getMax);

router.use(expressJwt({
    secret: config.secret,
    getToken: function (req) {
        if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
            return req.headers.authorization.split(' ')[1];
        } else if (req.query && req.query.token) {
            return req.query.token;
        }
        return null;
    }
}));
function getByID(req, res) {
    masterScheduleService.getByID(req.params.id).then(function (master) {
            res.send(master);
        }).catch(function (err) {
            res.status(400).send(err);
        });
}
function getMax(req, res) {
    masterScheduleService.getMax(req.params.id).then(function (max) {
            res.send(max);
        }).catch(function (err) {
            res.status(400).send(err);
        });
}
function getOptions(req, res) {
    masterScheduleService.getOptions(req.params).then(function (options) {
            res.send(options);
        }).catch(function (err) {
            res.status(400).send(err);
        });
}
module.exports = router;
