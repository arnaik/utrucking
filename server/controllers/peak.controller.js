var config = require('../config.json');
var express = require('express');
var router = express.Router();
var assert = require('assert');
var peakService = require('../services/peak.service');
const app = express();
const expressJwt = require('express-jwt');

router.get('/get_peak/:date', getPeak);

router.use(expressJwt({
    secret: config.secret,
    getToken: function (req) {
        if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
            return req.headers.authorization.split(' ')[1];
        } else if (req.query && req.query.token) {
            return req.query.token;
        }
        return null;
    }
}));

function getPeak(req, res) {
      peakService.getPeak(req.params.date).then(function (peak) {
            res.send(peak);
        }).catch(function (err) {
            res.send(400).send(err);
        });
}
module.exports = router;
