var config = require('../config.json');
var express = require('express');
var router = express.Router();
var assert = require('assert');
var settingService = require('../services/setting.service');
const app = express();
const expressJwt = require('express-jwt');

router.get('/', getSettings);

router.use(expressJwt({
    secret: config.secret,
    getToken: function (req) {
        if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
            return req.headers.authorization.split(' ')[1];
        } else if (req.query && req.query.token) {
            return req.query.token;
        }
        return null;
    }
}));

function getSettings(req, res) {
      settingService.getSettings().then(function (settings) {
            res.send(settings);
        }).catch(function (err) {
            res.send(400).send(err);
        });
}
module.exports = router;
