var config = require('../config.json');
var express = require('express');
var router = express.Router();
var assert = require('assert');
var eastCoastLocationService = require('../services/east_coast_location.service');
const app = express();
const expressJwt = require('express-jwt');

router.get('/', getAll);
router.get('/:id', getByID);

router.use(expressJwt({
    secret: config.secret,
    getToken: function (req) {
        if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
            return req.headers.authorization.split(' ')[1];
        } else if (req.query && req.query.token) {
            return req.query.token;
        }
        return null;
    }
}));

function getAll(req, res) {
  console.log("in controller of east coast ");
    eastCoastLocationService.getAll(req.body).then(function (locations) {
            res.send(locations);
        }).catch(function (err) {
            res.status(400).send(err);
        });
}
function getByID(req, res) {
    eastCoastLocationService.getByID(req.params.id).then(function (location) {
            res.send(location);
        }).catch(function (err) {
            res.send(400).send(err);
        });
}
module.exports = router;
