var config = require('../config.json');
var express = require('express');
var router = express.Router();
var assert = require('assert');
var addressService = require('../services/address.service');
const app = express();
const expressJwt = require('express-jwt');

router.post('/create', create);
router.get('/get_home_address/:id', getHomeAddress);
router.get('/get_shipping_address/:id', getShippingAddress);
router.get('/get_local_address/:id', getLocalAddress);
router.get('/get_billing_address/:id', getBillingAddress);
router.put('/update_address', updateAddress);

router.use(expressJwt({
    secret: config.secret,
    getToken: function (req) {
        if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
            return req.headers.authorization.split(' ')[1];
        } else if (req.query && req.query.token) {
            return req.query.token;
        }
        return null;
    }
}));
function create(req, res) {
    addressService.create(req.body).then(function (insertID) {
            res.send(insertID);
        }).catch(function (err) {
            res.status(400).send(err);
        });
}
function getHomeAddress(req, res) {
    addressService.getHomeAddress(req.params.id).then(function (homeAddress) {
            res.send(homeAddress);
        }).catch(function (err) {
            res.status(400).send(err);
        });
}
function getBillingAddress(req, res) {
    addressService.getBillingAddress(req.params.id).then(function (billingAddress) {
            res.send(billingAddress);
        }).catch(function (err) {
            res.status(400).send(err);
        });
}
function getShippingAddress(req, res) {
    addressService.getShippingAddress(req.params.id).then(function (shippingAddress) {
            res.send(shippingAddress);
        }).catch(function (err) {
            res.status(400).send(err);
        });
}
function getLocalAddress(req, res) {
    addressService.getLocalAddress(req.params.id).then(function (localAddress) {
            res.send(localAddress);
        }).catch(function (err) {
            res.status(400).send(err);
        });
}
function updateAddress(req, res) {
    addressService.updateAddress(req.body).then(function () {
            res.send(200);
        }).catch(function (err) {
            res.status(400).send(err);
        });
}
module.exports = router;
