var config = require('../config.json');
var express = require('express');
var router = express.Router();
var assert = require('assert');
var scheduleService = require('../services/schedule.service');
const app = express();
const expressJwt = require('express-jwt');

router.post('/create', create);
router.put('/update', update);
router.put('/update_master', updateMaster);
router.get('/getShipID/:userID', getShipID);
router.get('/getUserID/:userID', getUserID);
router.get('/get_count_per_ts/:id', getCountPerTimeSlot);
router.get('/get_count_per_day/:date', getCountPerDay);

router.use(expressJwt({
    secret: config.secret,
    getToken: function (req) {
        if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
            return req.headers.authorization.split(' ')[1];
        } else if (req.query && req.query.token) {
            return req.query.token;
        }
        return null;
    }
}));
function getShipID(req, res) {
    scheduleService.getShipID(req.params.userID).then(function (shippingAddrID) {
            res.send(shippingAddrID);
        }).catch(function (err) {
            res.status(400).send(err);
        });
}
function getUserID(req, res) {
    scheduleService.getUserID(req.params.userID).then(function (userID) {
            res.send(userID);
        }).catch(function (err) {
            res.status(400).send(err);
        });
}
function getCountPerTimeSlot(req, res) {
    scheduleService.getCountPerTimeSlot(req.params.id).then(function (count) {
            res.send(count);
        }).catch(function (err) {
            res.status(400).send(err);
        });
}
function getCountPerDay(req, res) {
    scheduleService.getCountPerDay(req.params.date).then(function (count) {
            res.send(count);
        }).catch(function (err) {
            res.status(400).send(err);
        });
}
function create(req, res) {
    scheduleService.create(req.body).then(function () {
            res.send(200);
        }).catch(function (err) {
            res.status(400).send(err);
        });
}
function update(req, res) {
    scheduleService.update(req.body).then(function () {
            res.send(200);
        }).catch(function (err) {
            res.status(400).send(err);
        });
}
function updateMaster(req, res) {
    scheduleService.updateMaster(req.body).then(function () {
            res.send(200);
        }).catch(function (err) {
            res.status(400).send(err);
        });
}
module.exports = router;
