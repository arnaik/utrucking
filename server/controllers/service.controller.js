var config = require('../config.json');
var express = require('express');
var router = express.Router();
var assert = require('assert');
var serviceService = require('../services/service.service');
const app = express();
const expressJwt = require('express-jwt');

router.get('/get_available', getAvailable);
router.get('/register', register);
router.get('/get_service/:id', getServiceByID);
router.get('/get_service_dates/:id', getServiceDates);

router.use(expressJwt({
    secret: config.secret,
    getToken: function (req) {
        if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
            return req.headers.authorization.split(' ')[1];
        } else if (req.query && req.query.token) {
            return req.query.token;
        }
        return null;
    }
}));
function getAvailable(req, res) {
    serviceService.getAvailable().then(function (services) {
            res.send(services);
        }).catch(function (err) {
            res.status(400).send(err);
        });
}
function getServiceByID(req, res) {
    serviceService.getServiceByID(req.params.id).then(function (service) {
            res.send(service);
        }).catch(function (err) {
            res.status(400).send(err);
        });
}
function getServiceDates(req, res) {
    serviceService.getServiceDates(req.params.id).then(function (serviceDates) {
            res.send(serviceDates);
        }).catch(function (err) {
            res.status(400).send(err);
        });
}
function register(req, res) {
    serviceService.create().then(function () {
            res.send(200);
        }).catch(function (err) {
            res.status(400).send(err);
        });
}
module.exports = router;
