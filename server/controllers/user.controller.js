var config = require('../config.json');
var express = require('express');
var router = express.Router();
var assert = require('assert');
var userService = require('../services/user.service');
const app = express();
const expressJwt = require('express-jwt');

router.post('/register', register);
router.post('/authenticate', authenticate);
router.get('/', getAll);
router.get('/:id', getByID);
router.put('/update_off_campus_info', updateOffCampusInfo);
router.put('/update_on_campus_info', updateOnCampusInfo);
router.put('/update_balance', updateBalance);
router.put('/update_scheduling', updateScheduling);
router.put('/update_password', updatePassword);
router.put('/update_pickup', updatePickup);
router.put('/update', updateAccount);
router.put('/update_billing', updateBilling);

router.use(expressJwt({
    secret: config.secret,
    getToken: function (req) {
        if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
            return req.headers.authorization.split(' ')[1];
        } else if (req.query && req.query.token) {
            return req.query.token;
        }
        return null;
    }
}));
function authenticate(req, res) {
    userService.authenticate(req.body.email, req.body.password)
        .then(function (user) {
            if (user) {
                // authentication successful
                res.send(user);
            } else {
                // authentication failed
                res.status(400).send('Email or password is incorrect');
            }
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}

function register(req, res) {
    userService.create(req.body).then(function () {
            res.send(200);
        }).catch(function (err) {
            res.status(400).send(err);
        });
}
function getAll(req, res) {
    userService.getAll().then(function (users) {
            res.send(users);
        }).catch(function (err) {
            res.status(400).send(err);
        });
}
function getByID(req, res) {
    userService.getByID(req.params.id).then(function (student) {
            res.send(student);
        }).catch(function (err) {
            res.status(400).send(err);
        });
}

function updateOffCampusInfo(req, res) {
    userService.updateOffCampusInfo(req.body).then(function () {
            res.send(200);
        }).catch(function (err) {
            res.status(400).send(err);
        });
}
function updateOnCampusInfo(req, res) {
    userService.updateOnCampusInfo(req.body).then(function () {
            res.send(200);
        }).catch(function (err) {
            res.status(400).send(err);
        });
}
function updateBalance(req, res) {
    userService.updateBalance(req.body).then(function () {
            res.send(200);
        }).catch(function (err) {
            res.status(400).send(err);
        });
}
function updateScheduling(req, res) {
    userService.updateScheduling(req.body).then(function () {
            res.send(200);
        }).catch(function (err) {
            res.status(400).send(err);
        });
}
function updatePassword(req, res) {
    userService.updatePassword(req.body).then(function () {
            res.send(200);
        }).catch(function (err) {
            res.status(400).send(err);
        });
}
function updatePickup(req, res) {
    userService.updatePickup(req.body).then(function () {
            res.send(200);
        }).catch(function (err) {
            res.status(400).send(err);
        });
}
function updateBilling(req, res) {
    userService.updateBilling(req.body).then(function () {
            res.send(200);
        }).catch(function (err) {
            res.status(400).send(err);
        });
}
function updateAccount(req, res) {
    userService.updateAccount(req.body).then(function () {
            res.send(200);
        }).catch(function (err) {
            res.status(400).send(err);
        });
}
module.exports = router;
