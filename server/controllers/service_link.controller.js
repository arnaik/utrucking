var config = require('../config.json');
var express = require('express');
var router = express.Router();
var assert = require('assert');
var serviceLinkService = require('../services/service_link.service');
const app = express();
const expressJwt = require('express-jwt');

router.post('/create', create);
router.get('/get_link/:service_id&:user_id', getLink);
router.get('/get_count/:id', getCount);

router.use(expressJwt({
    secret: config.secret,
    getToken: function (req) {
        if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
            return req.headers.authorization.split(' ')[1];
        } else if (req.query && req.query.token) {
            return req.query.token;
        }
        return null;
    }
}));

function create(req, res) {
    serviceLinkService.create(req.body).then(function () {
            res.send(200);
        }).catch(function (err) {
            res.status(400).send(err);
        });
}

function getLink(req, res) {
    serviceLinkService.getLink(req.params).then(function (serviceLink) {
            res.send(serviceLink);
        }).catch(function (err) {
            res.status(400).send(err);
        });
}
function getCount(req, res) {
    serviceLinkService.getCount(req.params.id).then(function (count) {
            res.send(count);
        }).catch(function (err) {
            res.status(400).send(err);
        });
}

module.exports = router;
