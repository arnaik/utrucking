const express = require('express');
const router = express.Router();
const MongoClient = require('mongodb').MongoClient;
const ObjectID = require('mongodb').ObjectID;
const mongoose = require('mongoose');
var Q = require('q');

// Connect
const connection = (closure) => {
    return MongoClient.connect('mongodb://localhost:27017/utrucking', (err, db) => {
        if (err) return console.log(err);

        closure(db);
    });
};

var mySchema = mongoose.Schema;
var userSchema = new mySchema({
  email: String,
  password: String,
  information: {
    first: String,
    last: String,
    parentEmail: String,
    homePhone: String,
    cellPhone: String,
    gradYear: Number
  },
  homeAddress: {
    street: String,
    city: String,
    state: String,
    country: String,
    zip: Number,
    province: String
  }
});
var User = mongoose.model('users', userSchema);
// Error handling
const sendError = (err, res) => {
    response.status = 501;
    response.message = typeof err == 'object' ? err.message : err;
    res.status(501).json(response);
};

// Response handling
let response = {
    status: 200,
    data: [],
    message: null
};

// Get users
router.get('/', (req, res) => {
    connection((db) => {
        db.collection('users')
            .find()
            .toArray()
            .then((users) => {
                response.data = users;
                res.json(response);
            })
            .catch((err) => {
                sendError(err, res);
            });
    });
});
router.get('/users', (req, res) => {
    connection((db) => {
        db.collection('users')
            .find()
            .toArray()
            .then((users) => {
                response.data = users;
                res.json(response);
            })
            .catch((err) => {
                sendError(err, res);
            });
    });
});

router.post('/register', (req, res) => {
  var newUser = req.body;

  connection((db) => {
    db.collection('users')
      .insertOne(newUser, (err, doc) => {
        if(err){
          sendError(err, res);
        }
        else{
          response.data = doc.ops[0];
          response.status = 201;
          res.json(response);
        }
      });
    });
});

module.exports = router;
