<!-- #Include File="../var.asp" --><%
ExtraHeadContent = "<script type=""text/javascript"">MainPage = ""Winter"";</script>"
%> <!-- #Include File="../top.asp" -->
<!-- Filename: winter/index.asp -->

<table width="450" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<br/><h2>Studying abroad for a semester?</h2><br/><h3>University Trucking is here to make your move-out experience as easy as <strong>A-B-C</strong>.</h3>
			If you are a new customer, just <a href="../signup.asp">create an account</a>, follow these three simple steps.
		</td>
	</tr>
	<tr>
		<td>
			<table width="450" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td width="450" height="20" colspan="4"><img src="images/spacer.gif" height="20" width="1" alt="" /></td>
				</tr>
				<tr>
					<td width="30"><img src="../images/spacer.gif" height="1" width="30" alt="" /></td>
					<td width="20"><img src="../images/spacer.gif" height="1" width="20" alt="" /></td>
					<td width="370"><img src="../images/spacer.gif" height="1" width="370" alt="" /></td>
					<td width="30"><img src="../images/spacer.gif" height="1" width="30" alt="" /></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td valign="top"><span class="header">A.</span></td>
					<td><a href = "../login.asp">Schedule a Pickup:</a> Log into your account or call  us  at
					<br />(314)-935-7691 to arrange a pickup for your storage and shipping.<br /></td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td width="450" height="15" colspan="4"><img src="images/spacer.gif" height="3" width="1" alt="" /></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td valign="top"><span class="header">B.</span></td>
					<td><a href="../supplies.asp">Get Your Free Boxes</a>: Come by out store and pickup your FREE boxes and tape and pack
					according to out <a href="../packing.asp">packing guidelines</a>.</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td width="450" height="15" colspan="4"><img src="images/spacer.gif" height="3" width="1" alt="" /></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td valign="top"><span class="header">C.</span></td>
					<td><u>FREE Room Pickup</u>: Sit back, relax, and wait for the professional movers of University Trucking to come to your room and pick up your belongings for study abroad storage or shipping home.</td>
					<td>&nbsp;</td>
				</tr>
			</table>
			<table>
				<tr>
					<td><h3>Please Click Below <br/>To Learn More About All Of Our Services And Prices:</h3></td>
				</tr>
			</table>
			<p align="center" >Services: &nbsp &nbsp <a href = "shipping.asp">Storage and Shipping</a> &nbsp &nbsp | &nbsp &nbsp <a href = "pricing.asp">Pricing</a> </p>
		</td>
	</tr>
</table>
<!-- #Include File="../bottom.asp" -->
