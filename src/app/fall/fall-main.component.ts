import { Component,Input, OnChanges, OnInit } from '@angular/core';
import { UserService } from '../_services/user.service';

@Component({
  selector: 'fall-main',
  templateUrl: './fall-main.component.html',
  styleUrls: ['./fall-main.component.css']
})
export class FallMainComponent implements OnChanges, OnInit{
  users: Array<any>;
  constructor(private userService: UserService){}
  ngOnInit(): void{
    this.userService.getAll()
        .subscribe(res => this.users = res);
  }
  ngOnChanges(): void{}
}
