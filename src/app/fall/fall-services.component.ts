import { Component, OnInit, Input} from '@angular/core';
import { LocationService } from '../_services/location.service';
import { Location } from '../models/location.model';
import { State } from '../models/state.model';
import { StateService } from '../_services/state.service';

@Component({
  selector: 'fall-services',
  templateUrl: './fall-services.component.html',
  styleUrls: ['./fall-services.component.css'],
  providers: [LocationService, StateService]
})
export class FallServicesComponent implements OnInit {
  id:number = 1;

  showMovingServices(): void{
    this.id = 1;
  }
  showNationwideServices(): void{
    //this.id = 2;
  }
  showApplianceRentals(): void{
    this.id = 3;
  }
  locations: Location[];
  states: State[];
  stateDescription: string;
  centerLat: number = 39.829022;
  centerLng: number = -98.579480;
  constructor(private locationService: LocationService, private stateService: StateService){
  }
  getLocations(): void {
    this.locations = this.locationService.getLocations();
  }
  getServiceStates(): void {
    this.states = this.stateService.getStates();
  }
  ngOnInit(): void{
    this.getLocations();
    this.getServiceStates();
  }
  filterByState(name: string): void{
    this.locations = this.locationService.filterByState(name);
    this.stateDescription = this.stateService.getDescription(name);
  }
  }
