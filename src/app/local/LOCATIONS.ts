import { Location } from '../models/location.model';


export const LOCATIONS: Location[] = [
  {
    id: 1,
    title: 'Newton South High School',
    latitude: 42.313611,
    longitude: -71.186028,
    date: 'August 7, 2017',
    address: "140 Brandeis Rd, Newton Centre, MA 02459",
    pathURL: 'https://www.google.com/maps/place/Newton+South+High+School/@42.3136457,-71.1884428,17z/data=!3m1!4b1!4m5!3m4!1s0x89e378a1738fe11b:0x3f5929ad1b1bba7d!8m2!3d42.313984!4d-71.1869516',
    state: "Massachusetts"
  },
  {
    id: 2,
    title: 'Westfarms Mall',
    latitude: 41.722347,
    longitude: -72.762709,
    date: 'August 7, 2017',
    address: "1500 New Britain Ave, West Hartford, CT 06110",
    pathURL: 'https://www.google.com/maps/place/Westfarms/@41.7229812,-72.763282,684m/data=!3m2!1e3!4b1!4m5!3m4!1s0x89e7ad9cb225d605:0x99e34f3d5fbaf13c!8m2!3d41.7229812!4d-72.763282?hl=en',
    state: "Connecticut"
},
  {
    id: 3,
    title: 'Greenwich Train Station',
    latitude: 41.021700,
    longitude: -73.625634,
    date: 'August 7, 2017',
    address: "Greenwich, CT 06830",
    pathURL: 'https://www.google.com/maps/place/Greenwich/@41.025321,-73.62462,1179m/data=!3m1!1e3!4m12!1m6!3m5!1s0x89c2984b78a40227:0xfa5a8e7f612d079!2sGreenwich!8m2!3d41.0213137!4d-73.6256185!3m4!1s0x89c2984b78a40227:0xfa5a8e7f612d079!8m2!3d41.0213137!4d-73.6256185?hl=en',
    state: "Connecticut"
},
  {
    id: 4,
    title: 'White Flint Mall',
    latitude: 39.042319,
    longitude: -77.107927,
    date: 'August 7, 2017',
    address: "11301 Rockville Pike, Kensington, MD 20895",
    pathURL: 'https://www.google.com/maps/place/White+Flint+Mall/@39.041815,-77.1102447,17z/data=!3m1!4b1!4m5!3m4!1s0x89b7cc216168e0e9:0xb0a1c9df47c08ab2!8m2!3d39.0421454!4d-77.107933',
    state: "Maryland"
},
  {
    id: 5,
    title: "Bed Bath and Beyond - Tyson's Corner",
    latitude: 38.917983,
    longitude: -77.233629,
    date: 'August 7, 2017',
    address: "2051 Chain Bridge Rd, Vienna, VA 22182",
    pathURL: 'https://www.google.com/maps/place/Bed+Bath+%26+Beyond/@38.920087,-77.221098,14z/data=!4m8!1m2!2m1!1sBed+Bath+%26+Beyond!3m4!1s0x0:0xa49e0895b9c3c254!8m2!3d38.9162057!4d-77.2341603?hl=en',
    state: "Virginia"
},
  {
    id: 6,
    title: 'Quaker Ridge School',
    latitude: 40.975103,
    longitude: -73.770447,
    date: 'August 8, 2017',
    address: "125 Weaver St, Scarsdale, NY 10583",
    pathURL: 'https://www.google.com/maps/place/Quaker+Ridge+Elementary+School/@40.984563,-73.774567,12z/data=!4m5!3m4!1s0x89c293c9cfa050c7:0x3711de4615093eec!8m2!3d40.9680123!4d-73.7710181?hl=en',
    state: "New York"
},
  {
    id: 7,
    title: 'Horace Greely High School',
    latitude: 41.175491,
    longitude: -73.757999,
    date: 'August 8, 2017',
    address: "70 Roaring Brook Rd, Chappaqua, NY 10514",
    pathURL: 'https://www.google.com/maps/place/Horace+Greeley+High+School/@41.1752353,-73.7601632,690m/data=!3m2!1e3!4b1!4m5!3m4!1s0x89c2b95dcf26aa35:0xe93f4ec4ef954e2e!8m2!3d41.1752353!4d-73.7579745?hl=en',
    state: "New York"
},
  {
    id: 8,
    title: 'Bala Cynwyd Shopping Center',
    latitude: 40.004591,
    longitude: -75.223689,
    date: 'August 8, 2017',
    address: "59 E City Ave, Bala Cynwyd, PA 19004",
    pathURL: 'https://www.google.com/maps/place/Bala+Cynwyd+Shopping+Center/@40.005561,-75.220728,15z/data=!4m5!3m4!1s0x89c6c760a2ea7465:0x131898d5b1dcc616!8m2!3d40.0037681!4d-75.2234753?hl=en',
    state: "Pennsylvania"
},
  {
    id: 9,
    title: 'East Brunswick Mall',
    latitude: 40.459031,
    longitude: -74.401476,
    date: 'August 8, 2017',
    address: "327 NJ-18, East Brunswick, NJ 08816",
    pathURL: 'https://www.google.com/maps/place/East+Brunswick+Shopping+Center/@40.450997,-74.394093,9514m/data=!3m1!1e3!4m8!1m2!2m1!1sEast+Brunswick+Shopping+Center,+New+Jersey+18,+East+Brunswick,+NJ!3m4!1s0x0:0xb005d92a6da3f1cf!8m2!3d40.45445!4d-74.4015384?hl=en',
    state: "New Jersey"
},
  {
    id: 10,
    title: "Livingston Mall - Macy's Lot",
    latitude: 40.777110,
    longitude: -74.353801,
    date: 'August 8, 2017',
    address: "112 Eisenhower Pkwy, Livingston, NJ 07039",
    pathURL: 'https://www.google.com/maps/place/Livingston+Mall/@40.776905,-74.3559777,592m/data=!3m2!1e3!4b1!4m5!3m4!1s0x89c3a928bcc5f563:0x6a5192fcab4fe0d!8m2!3d40.776901!4d-74.353789?hl=en',
    state: "New Jersey"
},
  {
    id: 11,
    title: "Garden State Plaza - Nordstrom Lot",
    latitude: 40.918376,
    longitude: -74.076299,
    date: 'August 8, 2017',
    address: "1 Garden State Plaza, Paramus, NJ 07652",
    pathURL: 'https://www.google.com/maps/place/Westfield+Garden+State+Plaza/@40.9181769,-74.0785197,17z/data=!3m1!4b1!4m5!3m4!1s0x89c2fa52b54a5ae7:0xc1d4874a74cf0472!8m2!3d40.9181729!4d-74.076331',
    state: "New Jersey"
},
  {
    id: 12,
    title: "Walt Whitman Mall",
    latitude: 40.822493,
    longitude: -73.410214,
    date: 'August 9, 2017',
    address: "160 Walt Whitman Rd, Huntington Station, NY 11746",
    pathURL: 'https://www.google.com/maps/place/Walt+Whitman+Shops/@40.8222614,-73.4123608,693m/data=!3m2!1e3!4b1!4m5!3m4!1s0x89e829045b4b3d01:0x2944b5875e8af323!8m2!3d40.8222614!4d-73.4101721?hl=en',
    state: "New York"
},
  {
    id: 13,
    title: "Green Acres Mall",
    latitude: 40.662829,
    longitude: -73.719295,
    date: 'August 9, 2017',
    address: "2034 Green Acres Mall, Sunrise Highway, Valley Stream, NY 11581",
    pathURL: 'https://www.google.com/maps/place/Green+Acres+Mall/@40.660978,-73.716738,1186m/data=!3m1!1e3!4m9!1m3!3m2!1s0x0:0x14db8c9ec2496dc6!2sgreen+acres+mall+Long+Island!3m4!1s0x0:0x7e77a9e355f9aa3!8m2!3d40.6624591!4d-73.7192917?hl=en',
    state: "New York"
},
  {
    id: 14,
    title: "Memorial City Mall - Macy's Lot",
    latitude: 29.781164,
    longitude: -95.539897,
    date: 'August 11, 2017',
    address: "303 Memorial City Way, Houston, TX 77024",
    pathURL: 'https://www.google.com/maps/place/Memorial+City+Mall/@29.780964,-95.5420807,17z/data=!3m1!4b1!4m5!3m4!1s0x8640c496256c7021:0x99a97c8c6601f4c3!8m2!3d29.780964!4d-95.539892',
    state: "Texas"
},
  {
    id: 15,
    title: "Barton Creek Square Mall - Macy's Lot",
    latitude: 30.258127,
    longitude: -97.807181,
    date: 'August 11, 2017',
    address: "2901 S Capital of Texas Hwy, Austin, TX 78746",
    pathURL: 'https://www.google.com/maps/place/Barton+Creek+Square/@30.2578655,-97.809391,17z/data=!3m1!4b1!4m5!3m4!1s0x865b4ae6e73f67c7:0x3e916e48f2b9e48f!8m2!3d30.2578655!4d-97.8072023?hl=en',
    state: "Texas"
},
  {
    id: 16,
    title: "NorthPark Center - Macy's Lot",
    latitude: 32.868696,
    longitude: -96.773438,
    date: 'August 12, 2017',
    address: "8687 N Central Expy, Dallas, TX 75225",
    pathURL: 'https://www.google.com/maps/place/NorthPark+Center/@32.8685063,-96.7757011,17z/data=!3m1!4b1!4m5!3m4!1s0x864e9f958a69f2e1:0x95fc10ba98f7aad4!8m2!3d32.8685018!4d-96.7735124',
    state: "Texas"
},
];
