import { Injectable } from '@angular/core';

@Injectable()
export class Student {
  constructor() {
    this.student_id = 0;
    this.first_name = "";
    this.last_name = "";
    this.student_email = "";
    this.password = "";
    this.home_addr_id = 0;
    this.local_addr_id = 0;
    this.billing_addr_id = 0;
    this.living_loc_id = 0;
    this.east_coast_location_id = 0;
    this.room = 0;
    this.home_phone = "";
    this.local_phone = "";
    this.cell_phone = "";
    this.grad_year = 0;
    this.ship_to_utrucking = 0;
    this.vault = 0;
    this.num_stored = 0;
    this.student_group = "";
    this.balance = 0;
    this.balance_text = "";
    this.boxes = 0;
    this.sched_price = 0;
    this.signup_date = "";
    this.parent_email = "";
  }
  student_id: number;
  first_name: string;
  last_name: string;
  student_email: string;
  password: string;
  home_addr_id: number;
  local_addr_id: number;
  billing_addr_id: number;
  living_loc_id: number;
  east_coast_location_id: number;
  room: number;
  home_phone: string;
  local_phone: string;
  cell_phone: string;
  grad_year: number;
  ship_to_utrucking: number;
  vault: number;
  num_stored: number;
  student_group: string;
  balance: number;
  balance_text: string;
  boxes: number;
  sched_price: number;
  signup_date: string;
  parent_email: string;
}
