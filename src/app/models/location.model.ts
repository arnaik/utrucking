export class Location{
  id: number;
  title: string;
  latitude: number;
  longitude: number;
  date: string;
  address: string;
  pathURL: string;
  state: string;
}
