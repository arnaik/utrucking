import { Injectable } from '@angular/core';

@Injectable()
export class ServiceLink {
  constructor() {}
  service_id: number;
  student_id: number;
  date: string;
}
