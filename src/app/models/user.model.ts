import { Injectable } from '@angular/core';

@Injectable()
export class User {
  constructor() {}
  email: string;
  password: string;
  information: {
    first: string;
    last: string;
    parentEmail: string;
    homePhone: string;
    cellPhone: string;
    localPhone: string;
    gradYear: number;
  };
  homeAddress: {
    street: string;
    city: string;
    state: string;
    country: string;
    zip: number;
    province: string;
  };
}
