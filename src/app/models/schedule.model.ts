import { Injectable } from '@angular/core';

@Injectable()
export class Schedule {
  constructor() {}
  schedule_id: number;
  student_id: number;
  master_schedule_id: number;
  shipping_addr_id: number;
}
