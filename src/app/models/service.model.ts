import { Injectable } from '@angular/core';

@Injectable()
export class Service {
  constructor() {}
  service_id: number;
  name: string;
  startDate: string;
  endDate: string;
  signupStartDate: string;
  signupEndDate: string;
  price: number;
  total: number;
  hidden: number;
  requireSchedule: number;
}
