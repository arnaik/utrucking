import { Injectable } from '@angular/core';

@Injectable()
export class MasterSchedule {
  constructor() {}
  master_schedule_id: number;
  truck_location_id: number;
  truck_id: number;
  date: string;
  start_time: number;
  end_time: number;
  max_number: number;
}
