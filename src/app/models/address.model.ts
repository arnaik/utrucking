export class Address{
  constructor() {
    this.address_id = 0;
    this.street = "";
    this.city = "";
    this.state = "";
    this.country = "";
    this.zip = "";
    this.province = "";
  }
  address_id: number;
  street: string;
  city: string;
  state: string;
  country: string;
  zip: string;
  province: string;
}
