import { Component, OnInit } from '@angular/core';
import { SeasonService } from './_services/season.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  season:string = '';
  constructor(private SeasonService: SeasonService) {
  }
  ngOnInit(): void{
    this.getSeason();
  }

  getSeason(): void {
    this.season = this.SeasonService.getSeason();
  }
}
