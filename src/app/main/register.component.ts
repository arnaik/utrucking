import { Component,Input, OnInit } from '@angular/core';
import { State } from '../models/state.model';
import { StateService } from '../_services/state.service';
import { Student } from '../models/student.model';
import { Address } from '../models/address.model';
import { AlertService } from '../_services/alert.service';
import { UserService } from '../_services/user.service';
import { CountryService } from '../_services/country.service';
import { AddressService } from '../_services/address.service';
import { Router } from '@angular/router';

@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterUserComponent implements OnInit {
  student = new Student();
  address = new Address();
  states: State[] = [];
  countries: String[] = [];
  confirmedPassword: any = "";
  constructor(private router: Router, private userService: UserService, private stateService: StateService, private alertService: AlertService, private countryService: CountryService, private addressService: AddressService){}
  ngOnInit(): void{
    this.getStates();
    this.getCountries();
  }
  getCountries(): void{
    this.countries = this.countryService.getCountries();
  }
  getStates(): void{
    this.states = this.stateService.getStates();
  }
  createUser(): void{
    this.handleErrors();
    this.addressService.createAddress(this.address).subscribe(
      data => {
        this.student.home_addr_id = data.insertID;
        console.log(this.student.home_addr_id);
        console.log(this.student);
        this.userService.create(this.student)
            .subscribe(
              data => {
                sessionStorage.setItem('currentUser', JSON.stringify(user));
                this.router.navigate(['/profile']);
                this.alertService.success('Registration successful', true);
              },
              error => {
                this.alertService.error(error);
              }
            );
      },
      error => {
        this.alertService.error(error);
      }
    );
  }
  handleErrors(): void{
    if(this.student.student_email.length == 0 || this.student.password.length == 0
      || this.student.first_name.length == 0 || this.student.last_name.length == 0
      || this.student.parent_email.length == 0 || this.student.home_phone.length == 0
      || this.student.cell_phone.length == 0 || this.student.grad_year == 0
      || this.address.street.length == 0 || this.address.city.length == 0
      || this.address.state.length == 0 || this.address.country.length == 0
      || this.address.zip.length == 0){
      this.alertService.error("Please make sure to fill all required fields");
    }
    if(this.student.password != this.confirmedPassword){
      this.alertService.error("Please make sure the passwords match");
    }
  }
}
