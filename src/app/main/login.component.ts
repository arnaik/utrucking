import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AlertService } from '../_services/alert.service';
import { AuthenticationService } from '../_services/authentication.service';

@Component({
    selector: 'login',
    templateUrl: 'login.component.html'
})
export class LoginComponent {
  user: any = {};
  constructor(private route: ActivatedRoute, private router: Router, private authenticationService: AuthenticationService, private alertService: AlertService) { }
  ngOnInit() {
        // reset login status
        this.authenticationService.logout();
    }
  login() {
    this.authenticationService.login(this.user.email, this.user.password)
      .subscribe(
        data => {
          this.router.navigate(['/profile']);
          this.alertService.success('Login successful', true);
        },
        error => {
            this.alertService.error(error);
          });
   }
}
