import { Component, OnInit } from '@angular/core';
import { Service } from '../models/service.model';
import { User } from '../models/user.model';
import { Address } from '../models/address.model';
import { Setting } from '../models/setting.model';
import { ServiceService } from '../_services/service.service';
import { UserService } from '../_services/user.service';
import { AlertService } from '../_services/alert.service';
import { ServiceLinkService } from '../_services/service_link.service';
import { MasterScheduleService } from '../_services/master_schedule.service';
import { LivingLocationService } from '../_services/living_location.service';
import { AddressService } from '../_services/address.service';
import { ScheduleService } from '../_services/schedule.service';
import { PeakService } from '../_services/peak.service';
import { SettingService } from '../_services/setting.service';
import { StateService } from '../_services/state.service';
import { CountryService } from '../_services/country.service';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { State } from '../models/state.model';

@Component({
  selector: 'edit_account',
  templateUrl: './edit_account.component.html',
  styleUrls: ['./edit_account.component.css'],
})
export class EditAccountComponent implements OnInit{
  states: State[] = [];
  countries: String[] = [];
  student: any;
  homeAddress: any;
  constructor (private serviceService: ServiceService, private router: Router, private userService: UserService, private alertService: AlertService, private serviceLinkService: ServiceLinkService, private masterScheduleService: MasterScheduleService, private addressService: AddressService, private livingLocationService: LivingLocationService, private scheduleService: ScheduleService, private peakService: PeakService, private settingService: SettingService, private countryService: CountryService, private stateService: StateService){}
  ngOnInit(): void{
    this.getUserInfo();
    this.countries = this.countryService.getCountries();
    this.states = this.stateService.getStates();
 }
 getUserInfo(){
   this.userService.getByID(JSON.parse(sessionStorage.getItem('currentUser')).id).subscribe(
     data => {
       this.student = data[0];
       this.addressService.getHomeAddress(JSON.parse(sessionStorage.getItem('currentUser')).id).subscribe(
         data => {
           this.homeAddress = data;
         },
         error => {
           this.alertService.error(error);
         }
       );
     },
     error => {
       this.alertService.error(error);
     }
   );
 }
 updateInfo(){
   this.userService.update(this.student).subscribe(
     data => {
       console.log("success!");
     },
     error => {
       this.alertService.error(error);
     }
   );
   this.addressService.updateAddress(this.homeAddress).subscribe(
     data => {
       console.log("success!");
     },
     error => {
       this.alertService.error(error);
     }
   );
 }
}
