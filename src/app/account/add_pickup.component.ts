import { Component, OnInit } from '@angular/core';
import { Service } from '../models/service.model';
import { LivingLocation } from '../models/living_location.model';
import { ServiceService } from '../_services/service.service';
import { AddressService } from '../_services/address.service';
import { ServiceLinkService } from '../_services/service_link.service';
import { LivingLocationService } from '../_services/living_location.service';
import { AlertService } from '../_services/alert.service';
import { UserService } from '../_services/user.service';
import { StateService } from '../_services/state.service';
import { CountryService } from '../_services/country.service';
import { ScheduleService } from '../_services/schedule.service';
import { Address } from '../models/address.model';
import { State } from '../models/state.model';
import { Schedule } from '../models/schedule.model';
import { User } from '../models/user.model';
import { ServiceLink } from '../models/service_link.model';
import { ActivatedRoute, Router } from '@angular/router';
import { MasterScheduleService } from '../_services/master_schedule.service';

@Component({
  selector: 'add_pickup',
  templateUrl: './add_pickup.component.html',
  styleUrls: ['./add_pickup.component.css'],
})
export class AddPickupComponent implements OnInit{
  service: any = {
    name: ""
  };
  serviceID = 0;
  offCampusAddress: any = {
    street: "",
    city: "St. Louis",
    state: "MO",
    country: "United States of America",
    zip: "63130",
    province: ""
  }
  homeAddress: any = {
    street: ""
  };
  newAddrBool = 0;
  oldAddrBool = 1;
  newAddress: any = {
    street: "",
    city: "",
    state: "",
    country: "",
    zip: "",
    province: ""
  };
  params: any = {};
  livingLocations: any;
  livingLocationID = 0;
  livingLocationName = "Building Name";
  room: number = 0;
  shippingAddressID = 0;
  states: State[] = [];
  countries: String[] = [];
  schedule: Schedule = {
    schedule_id: 0,
    student_id: JSON.parse(sessionStorage.getItem('currentUser')).id,
    master_schedule_id: 0,
    shipping_addr_id: 0
  };
  serviceLink: any = {
    service_id: 0,
    user_id: 0
  }
  showAll = true;
  showShipping = true;
  student: any = {
    studentID: 0,
    balance: 0,
    balanceText: ""
  };
  constructor (private serviceService: ServiceService, private addressService: AddressService, private route: ActivatedRoute, private router: Router, private serviceLinkService: ServiceLinkService, private livingLocationService: LivingLocationService, private alertService: AlertService, private userService: UserService, private stateService: StateService, private countryService: CountryService, private scheduleService: ScheduleService, private masterScheduleService: MasterScheduleService){}
  ngOnInit(): void{
    this.route
   .queryParams
   .subscribe(params => {
     // Defaults to 0 if no query param provided.
     this.serviceID = +params['serviceID'] || 0;
   });
   var nowTimestamp;
   var thenTimestamp;
   this.masterScheduleService.getByID(JSON.parse(sessionStorage.getItem('currentUser')).id).subscribe(
     data => {
       if(data.result == "EOF"){
         nowTimestamp = 1;
         thenTimestamp = 2;
       }else{
         let hours = 36;
         nowTimestamp = (new Date).getTime() + (3600 * (2 + hours));
         thenTimestamp = (new Date(data.result.date + " " + data.result.start_time + ":00:00")).getTime();
       }
       if(nowTimestamp > thenTimestamp){
         this.router.navigate(['/profile']);
         this.alertService.success("It is too close to the scheduled date and time for this service to order it");
       }
     },
     error => {
       this.alertService.error(error);
     });

     this.userService.getByID(JSON.parse(sessionStorage.getItem('currentUser')).id).subscribe(
     data => {
       this.student.studentID = data[0].student_id;
       this.student.balance = data[0].balance;
       this.student.balanceText = data[0].balance_text;
     },
     error => {
       this.alertService.error(error);
     });
   //check to see if service has been ordered
   this.checkIfOrdered();
   //check to see if any other services have been ordered
   this.checkIfOrderedOtherServices();
   //get deposit,price, and name of service
   if(this.serviceID){
     this.serviceService.getServiceByID(this.serviceID).subscribe(
       data => {
         this.service = data;
       },
       error => {
         this.alertService.error(error);
       });
   }
   this.livingLocationService.getAll().subscribe(response => this.livingLocations = response);
   this.getStates();
   this.getCountries();
   this.addressService.getHomeAddress(JSON.parse(sessionStorage.getItem('currentUser')).id)
   .subscribe(response => this.homeAddress = response);
  }
  checkIfOrderedShippingServices(){
    this.scheduleService.getShipID(JSON.parse(sessionStorage.getItem('currentUser')).id).subscribe(
      data => {
        if(data.result == "EOF"){
          this.showShipping = this.showAll;
        }
        else{
          this.showShipping = this.showAll || (data.result == 0);
        }
      },
      error => {
        this.alertService.error(error);
      }
    );
  }
  checkIfOrderedOtherServices(){
    this.scheduleService.getUserID(JSON.parse(sessionStorage.getItem('currentUser')).id).subscribe(
      data => {
        if(data.result == "EOF"){
          this.showAll = true;
        }
        else{
          this.showAll = false;
        }
        //check to see if other shipping services have been ordered
        this.checkIfOrderedShippingServices();
      },
      error => {
        this.alertService.error(error);
      }
    );
  }
  checkIfOrdered(){
    this.serviceLink.service_id = this.serviceID;
    this.serviceLink.user_id = JSON.parse(sessionStorage.getItem('currentUser')).id;
    this.serviceLinkService.getServiceLink(this.serviceLink).subscribe(
      data => {
        if(data.result == "ALREADY_EXISTS"){
          this.router.navigate(['/profile']);
          this.alertService.success('This service has already been ordered');
        }

      },
      error => {
        this.alertService.error(error);
      }
    );
  }
  getCountries(): void{
    this.countries = this.countryService.getCountries();
  }
  getStates(): void{
    this.states = this.stateService.getStates();
  }
  setLivingLocation(livingLocationID: number, livingLocationName: string){
    this.livingLocationID = livingLocationID;
    this.livingLocationName = livingLocationName;
  }
  useHomeAddress() {
    this.newAddrBool = 0;
    this.oldAddrBool = 1;
  }

  addAddress() {
    this.oldAddrBool = 0;
    this.newAddrBool = 1;
  }
  createService(){
    //insert service_link
    this.params = {
      serviceID: this.serviceID,
      userID: JSON.parse(sessionStorage.getItem('currentUser')).id
    };
    this.serviceLinkService.addServiceLink(this.params).subscribe(
      data => {
        console.log("added service link");
      },
      error => {
        this.alertService.error(error);
      }
    );
    //on/off campus
    if(this.livingLocationID == 1){
      this.addressService.createAddress(this.offCampusAddress).subscribe(
      data => {
        this.updateOffCampusInfo(data.insertID);
        },
      error => {
        this.alertService.error(error);
        }
      );
    }else if(this.livingLocationID > 1){
      this.updateOnCampusInfo();
    }
    //old/new shipping address
    if(this.oldAddrBool == 1){
      this.shippingAddressID = this.homeAddress.address_id;
    }
    else{
      this.addressService.createAddress(this.newAddress).subscribe(
      data => {
        this.shippingAddressID = data.insertID;
        },
      error => {
        this.alertService.error(error);
        }
      );
    }
    //add Schedule
    this.scheduleService.getShipID(JSON.parse(sessionStorage.getItem('currentUser')).id).subscribe(
      data => {
        this.schedule.shipping_addr_id = this.shippingAddressID;
        if(data.result == "EOF"){
          this.createNewSchedule(this.schedule);
        }
        else if(data.result == 0){
          this.updateSchedule(this.schedule);
        }
      },
      error => {
        this.alertService.error(error);
      }
    );
    //email service


    //add balance and balance text
    if(this.service.price + this.service.deposit > 0){
      this.student.balance = this.student.balance + this.service.price + this.service.deposit;
      if(this.student.balanceText == null){
        this.student.balanceText = this.service.name;
      }else{
        this.student.balanceText = this.student.balanceText + ", " + this.service.name;
      }
      if(this.student.balance == 0){
        this.student.balanceText == "";
      }
    }
    this.userService.updateBalance(this.student).subscribe(
      data => {
        console.log("updated balance and balance text");
      },
      error => {
        this.alertService.error(error);
      }
    );
  }
  createNewSchedule(schedule){
    console.log(schedule);
    this.scheduleService.addSchedule(schedule).subscribe(
      data => {
        console.log("added new schedule");
      },
      error => {
        this.alertService.error(error);
      }
    );
  }
  updateSchedule(schedule){
    this.scheduleService.updateSchedule(schedule).subscribe(
      data => {
        console.log("updated schedule");
      },
      error => {
        this.alertService.error(error);
      }
    );
  }
  updateOffCampusInfo(offCampusInsertID){
    this.userService.updateOffCampusInfo(
      {
        userID: JSON.parse(sessionStorage.getItem('currentUser')).id,
        livingLocationID: this.livingLocationID,
        localAddressID: offCampusInsertID
      }
  ).subscribe(
      data => {
        console.log("updated off campus info successfully");
      },
      error => {
        this.alertService.error(error);
      }
  );
  }
  updateOnCampusInfo(){
    this.userService.updateOnCampusInfo(
      {
        userID: JSON.parse(sessionStorage.getItem('currentUser')).id,
        livingLocationID: this.livingLocationID,
        room: this.room
      }
    ).subscribe(
        data => {
          console.log("updated on campus info successfully");
        },
        error => {
          this.alertService.error(error);
        }
    );
  }
}
