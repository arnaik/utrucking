import { Component, OnInit } from '@angular/core';
import { Service } from '../models/service.model';
import { Address } from '../models/address.model';
import { Setting } from '../models/setting.model';
import { ServiceService } from '../_services/service.service';
import { UserService } from '../_services/user.service';
import { AlertService } from '../_services/alert.service';
import { ServiceLinkService } from '../_services/service_link.service';
import { MasterScheduleService } from '../_services/master_schedule.service';
import { LivingLocationService } from '../_services/living_location.service';
import { AddressService } from '../_services/address.service';
import { ScheduleService } from '../_services/schedule.service';
import { PeakService } from '../_services/peak.service';
import { SettingService } from '../_services/setting.service';
import { StateService } from '../_services/state.service';
import { CountryService } from '../_services/country.service';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { State } from '../models/state.model';

@Component({
  selector: 'edit_schedule',
  templateUrl: './edit_schedule.component.html',
  styleUrls: ['./edit_schedule.component.css'],
})
export class EditScheduleComponent implements OnInit{
  editScheduleBool = false;
  actionText: String;
  masterSchedule: any;
  noSchedule = false;
  closeToSchedule = false;
  noMasterScheduleBool = false;
  startTime: String;
  endTime: String;
  options: Array<{
    master_schedule_id:number,
    date: string,
    start_time:string,
    end_time:string,
    schedule_price:number
  }>;
  specOptions: Array<{
      master_schedule_id:number,
      date: string,
      start_time: string,
      end_time:string,
      schedule_price:number
  }> = [];
  settings: Setting[];
  student: any = {
    student_id: 0,
    first_name: "",
    last_name: "",
    balance: 0,
    balance_text: "",
    room: 0,
    living_loc_id: 0,
    local_addr_id: 0,
    sched_price: 0
  };
  chooseTimeSlotBool = false;
  myOption: any;
  paidSchedulePrice = 0;
  beforeStartTimeDate = false;

constructor (private serviceService: ServiceService, private router: Router, private userService: UserService, private alertService: AlertService, private serviceLinkService: ServiceLinkService, private masterScheduleService: MasterScheduleService, private addressService: AddressService, private livingLocationService: LivingLocationService, private scheduleService: ScheduleService, private peakService: PeakService, private settingService: SettingService, private countryService: CountryService, private stateService: StateService){}

ngOnInit(): void{
  this.getUserInfo();
  this.getSettings();
  var nowTimestamp;
  var thenTimestamp;
  this.masterScheduleService.getByID(JSON.parse(sessionStorage.getItem('currentUser')).id).subscribe(
    data => {
      this.masterSchedule = data.result;
      this.editSchedule();
    },
    error => {
      this.alertService.error(error);
    });
}
getUserInfo(){
  this.userService.getByID(JSON.parse(sessionStorage.getItem('currentUser')).id).subscribe(
    data => {
      this.student = data[0];
      this.paidSchedulePrice = this.student.sched_price;
    },
    error => {
      this.alertService.error(error);
    }
  );
}

editSchedule(){
  let month = (new Date()).getMonth();
  if(month >= 6 && month <= 9){
    this.actionText = "delivered to";
  }else{
    this.actionText = "picked up from";
  }
  if(this.masterSchedule == "EOF"){
    this.noMasterScheduleBool = true;
  }
  else{
        this.startTime = this.timeFromDB(this.masterSchedule.start_time);
        this.endTime = this.timeFromDB(this.masterSchedule.end_time);
  }
  //get service dates
  var startDate;
  var endDate;
  this.serviceService.getServiceDates(JSON.parse(sessionStorage.getItem('currentUser')).id).subscribe(
    data => {
      if(data.result != "EOF"){
        startDate = data.result.start_date;
        endDate = data.result.end_date;
        this.masterScheduleService.getOptions({
          id: JSON.parse(sessionStorage.getItem('currentUser')).id,
          start_date: (new Date(startDate)).toISOString().substring(0,10),
          end_date: (new Date(endDate)).toISOString().substring(0,10)
        }).subscribe(
          data => {
            if(data.result == "EOF"){
            }else{
              this.options = data.result;
              this.getOptionSpecs();
            }
          },
          error => {
            this.alertService.error(error);
          }
        );
      }
    },
    error => {
      this.alertService.error(error);
    }
  );

}
getOptionSpecs(){
  var counter = 0;
  for (let option of this.options){
    this.masterScheduleService.getMax(option.master_schedule_id).subscribe(
      data => {
        if(data.result != "EOF"){
          this.scheduleService.getCountPerTimeSlot(option.master_schedule_id).subscribe(
            data => {
              var count;
              var availableMasterSchedule = false;
              if(data.result == "EOF"){
                availableMasterSchedule = true;
              }else{
                count = data.result.count;
                availableMasterSchedule = count < 6
              }
              if(availableMasterSchedule){
                let hours = 48;
                let nowTimestamp = (new Date()).getTime() + (3600 * (hours));
                let thenTimestamp = (new Date(option.date + " " + option.start_time + ":00:00")).getTime();
                var schedulePrice = 0;
                if(nowTimestamp <= thenTimestamp){
                  this.peakService.getPeak((new Date(option.date)).toISOString().substring(0,10)).subscribe(
                    data => {
                      if(data.result.price){
                        schedulePrice = data.result.price;
                        this.specOptions.push(option);
                        this.specOptions[counter].start_time = this.timeFromDB(parseInt(this.specOptions[counter].start_time));
                        this.specOptions[counter].end_time = this.timeFromDB(parseInt(this.specOptions[counter].end_time));
                        this.specOptions[counter].schedule_price = schedulePrice;
                        counter = counter + 1;
                      }
                      else if(data.result == "EOF"){
                        this.scheduleService.getCountPerDay((new Date(option.date)).toISOString().substring(0,10)).subscribe(
                          data => {
                            var overflowNumber = this.settings[0].value;
                            var overflowPrice = this.settings[1].value;
                            if(overflowNumber < data.count){
                              schedulePrice = overflowPrice;
                            }
                            this.specOptions.push(option);
                            this.specOptions[counter].schedule_price = schedulePrice;
                            this.specOptions[counter].start_time = this.timeFromDB(parseInt(this.specOptions[counter].start_time));
                            this.specOptions[counter].end_time = this.timeFromDB(parseInt(this.specOptions[counter].end_time));
                            counter = counter + 1;
                          },
                          error => {
                            this.alertService.error(error);
                          }
                        );
                      }
                    },
                    error => {
                      this.alertService.error(error);
                    }
                  );
                }
              }
            },
            error => {
              this.alertService.error(error);
            }
          )
        }
      },
      error => {
        this.alertService.error(error);
      }
    );
  }
}
selectTimeSlot(){
  let oldBalanceText = this.student.balance_text;
  var newBalanceText;
  if(this.myOption.schedule_price != this.paidSchedulePrice){
    if(this.myOption.schedule_price > 0 && this.paidSchedulePrice == 0){
      if(this.student.balance_text){
        this.student.balance_text = this.student.balance_text + ", " + "Peak Scheduling Charge";
      }else{
        this.student.balance_text = "Peak Scheduling Charge";
      }
    }
    else if(this.myOption.schedule_price == 0 && this.paidSchedulePrice > 0){
      for(let service of oldBalanceText.split(", ")){
        if(service != "Peak Scheduling Charge"){
          if(newBalanceText){
            newBalanceText = newBalanceText + ", " + service;
          }else{
            newBalanceText = service;
          }
        }
      }
    }
    this.userService.updateScheduling({
      balance: this.student.balance - this.paidSchedulePrice + this.myOption.schedule_price,
      balance_text: newBalanceText || this.student.balance_text,
      sched_price: this.myOption.schedule_price,
      student_id: JSON.parse(sessionStorage.getItem('currentUser')).id
    }).subscribe(
      data => {
        console.log("updated student schedule");
      },
      error => {
        this.alertService.error(error);
      }
    );
  }
  this.scheduleService.updateMaster({
    master_schedule_id: this.myOption.master_schedule_id,
    student_id: JSON.parse(sessionStorage.getItem('currentUser')).id
  }).subscribe(
    data => {
      this.editScheduleBool = true;
    },
    error => {
      this.alertService.error(error);
    }
  );
}
chooseTimeSlot(option){
  this.chooseTimeSlotBool = true;
  this.myOption = option;
}
timeFromDB(dbTime): string{
  var returnString;
  if((dbTime - 12) > 0){
      returnString = (dbTime - 12) + ":00 PM";
  }
  else if((dbTime - 12) == 0){
    returnString = dbTime + ":00 PM";
  }
  else{
    returnString = dbTime + ":00AM";
  }
  return returnString;
}

getSettings(): void{
  this.settingService.getSettings().subscribe(response => this.settings = response);
}
}
