import { Component, OnInit } from '@angular/core';
import { AlertService } from '../_services/alert.service';
import { ActivatedRoute, Router } from '@angular/router';
import { PaymentService } from '../_services/payment.service';

@Component({
  selector: 'list_payment',
  templateUrl: './list_payment.component.html',
  styleUrls: ['./list_payment.component.css'],
})
export class ListPaymentComponent implements OnInit{
  payments: any;

  constructor (private alertService: AlertService, private paymentService: PaymentService){}
  ngOnInit(): void{
    this.paymentService.getByID(JSON.parse(sessionStorage.getItem('currentUser')).id).subscribe(
      data => {
        this.payments = data;
        console.log(this.payments);
      },
      error => {
        this.alertService.error(error);
      });
  }
}
