import { Component, OnInit } from '@angular/core';
import { Service } from '../models/service.model';
import { User } from '../models/user.model';
import { Address } from '../models/address.model';
import { Setting } from '../models/setting.model';
import { ServiceService } from '../_services/service.service';
import { UserService } from '../_services/user.service';
import { AlertService } from '../_services/alert.service';
import { ServiceLinkService } from '../_services/service_link.service';
import { MasterScheduleService } from '../_services/master_schedule.service';
import { LivingLocationService } from '../_services/living_location.service';
import { AddressService } from '../_services/address.service';
import { ScheduleService } from '../_services/schedule.service';
import { PeakService } from '../_services/peak.service';
import { SettingService } from '../_services/setting.service';
import { StateService } from '../_services/state.service';
import { CountryService } from '../_services/country.service';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { State } from '../models/state.model';

@Component({
  selector: 'edit_password',
  templateUrl: './edit_password.component.html',
  styleUrls: ['./edit_password.component.css'],
})
export class EditPasswordComponent implements OnInit{
  newPassword: any;
  newPasswordConfirm: any;
  student: any;
  constructor (private serviceService: ServiceService, private router: Router, private userService: UserService, private alertService: AlertService, private serviceLinkService: ServiceLinkService, private masterScheduleService: MasterScheduleService, private addressService: AddressService, private livingLocationService: LivingLocationService, private scheduleService: ScheduleService, private peakService: PeakService, private settingService: SettingService, private countryService: CountryService, private stateService: StateService){}
  ngOnInit(): void{
    this.getUserInfo();
 }
 getUserInfo(){
   this.userService.getByID(JSON.parse(sessionStorage.getItem('currentUser')).id).subscribe(
     data => {
       this.student = data[0];
     },
     error => {
       this.alertService.error(error);
     }
   );
 }
 updatePassword(){
   if(this.newPassword != this.newPasswordConfirm){
     this.alertService.error("The confirmed password you entered does not match");
   }
   else{
     this.student.password = this.newPassword;
     this.userService.updatePassword(this.student).subscribe(
       data => {
         console.log("password has been changed!");
         this.router.navigate(['/profile']);
       },
       error => {
         this.alertService.error(error);
       }
     );
   }
 }
}
