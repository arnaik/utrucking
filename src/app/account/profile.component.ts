import { Component, OnInit } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
//models
import { Service } from '../models/service.model';
import { Address } from '../models/address.model';
import { Setting } from '../models/setting.model';
import { State } from '../models/state.model';
//services
import { ServiceService } from '../_services/service.service';
import { UserService } from '../_services/user.service';
import { AlertService } from '../_services/alert.service';
import { ServiceLinkService } from '../_services/service_link.service';
import { MasterScheduleService } from '../_services/master_schedule.service';
import { LivingLocationService } from '../_services/living_location.service';
import { AddressService } from '../_services/address.service';
import { ScheduleService } from '../_services/schedule.service';
import { PeakService } from '../_services/peak.service';
import { SettingService } from '../_services/setting.service';
import { StateService } from '../_services/state.service';
import { CountryService } from '../_services/country.service';
import { EastCoastLocationService } from '../_services/east_coast_location.service';
import { AuthenticationService } from '../_services/authentication.service';

@Component({
  selector: 'profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
})
export class ProfileComponent implements OnInit{
  states: any = {};
  countries: any = {}
  potentialServices: Array<any> = [];
  availableServices: Array<any> = [];
  serviceType: any;
  orderedServices: Array<any> = [];
  student: any = {};
  inDebt = false;
  beforeStartTimeDate = false;
  masterSchedule: any;
  noSchedule = false;
  closeToSchedule = false;
  shippingAddress: Address;
  onCampus: any;
  onCampusAddr: any = {};
  offCampusAddr: Address;
  editScheduleBool = false;
  actionText: String;
  noMasterScheduleBool = false;
  startTime: String;
  endTime: String;
  settings: Setting[];
  editLocalBool = false;
  livingLocations: any;
  livingLocationID = 0;
  livingLocationName = "Building Name";
  newOffCampusAddr = {
    street: "",
    city: "St. Louis",
    state: "Missouri",
    country: "United States of America",
    province: "",
    zip: "63130"
};
  eastCoastLocationName: any;
  constructor (private serviceService: ServiceService, private router: Router, private userService: UserService, private alertService: AlertService, private serviceLinkService: ServiceLinkService, private masterScheduleService: MasterScheduleService, private addressService: AddressService, private livingLocationService: LivingLocationService, private scheduleService: ScheduleService, private peakService: PeakService, private settingService: SettingService, private countryService: CountryService, private stateService: StateService, private eastCoastLocationService: EastCoastLocationService, private authenticationService: AuthenticationService){}
  ngOnInit(): void{
    this.getStudentInfo();
    this.getSettings();
    this.getLivingLocations();
    this.getCountries();
    this.getStates();
    this.getMasterSchedule();
  }
  getStudentInfo(): void {
    this.userService.getByID(JSON.parse(sessionStorage.getItem('currentUser')).id).subscribe(
      data => {
        this.student = data[0];
        if(this.student.balance > 0){
          this.inDebt = true;
        }
      },
      error => {
        this.alertService.error(error);
      }
    );
  }

  getSettings(): void {
    this.settingService.getSettings().subscribe(response => this.settings = response);
  }

  getLivingLocations(): void {
    this.livingLocationService.getAll().subscribe(response => this.livingLocations = response);
  }

  getCountries(): void{
    this.countries = this.countryService.getCountries();
  }

  getStates(): void{
    this.states = this.stateService.getStates();
  }

  getMasterSchedule(): void{
    var nowTimestamp;
    var thenTimestamp;
    this.masterScheduleService.getByID(JSON.parse(sessionStorage.getItem('currentUser')).id).subscribe(
      data => {
        this.masterSchedule = data.result;
        if(data.result == "EOF"){
          nowTimestamp = 1;
          thenTimestamp = 2;
        }else if(data.result.date){
          let hours = 36;
          nowTimestamp = (new Date).getTime() + (3600 * (2 + hours));
          thenTimestamp = (new Date(data.result.date + " " + data.result.start_time + ":00:00")).getTime();
        }
        if(nowTimestamp <= thenTimestamp){
          this.beforeStartTimeDate = true;
        }
        this.getAvailableServices();
        this.showOrderedServiceInfo();
      },
      error => {
        this.alertService.error(error);
      });
  }

  getAvailableServices(): void{
    this.serviceService.getAvailable().subscribe(
      data => {
        this.potentialServices = data;
        this.checkIfOrdered();
      },
      error => {
        this.alertService.error(error);
      });
  }

  showOrderedServiceInfo(): void{
    var nowTimestamp;
    var thenTimestamp;
    if(this.masterSchedule == "EOF"){
      this.noSchedule = true;
    }else{
      this.noSchedule = false;
    }
    let hours = 36;
    nowTimestamp = (new Date).getTime() + (3600 * (2 + hours));
    thenTimestamp = (new Date(this.masterSchedule.date + " " + this.masterSchedule.start_time + ":00:00")).getTime();
    if(nowTimestamp <= thenTimestamp){
      this.closeToSchedule = false;
    }else{
      this.closeToSchedule = true;
    }
    //get shipping address
    this.addressService.getShippingAddress(JSON.parse(sessionStorage.getItem('currentUser')).id).subscribe(
      data => {
        if(data.result != "EOF"){
          this.shippingAddress = data.result;
        }
      },
      error => {
        this.alertService.error(error);
      });

    //get living location
    if(this.student.room != 0){
      this.livingLocationService.getLocation(JSON.parse(sessionStorage.getItem('currentUser')).id).subscribe(
        data => {
          if(data.result.room != 0){
            this.onCampusAddr = data.result;
            this.onCampusAddr.room = this.student.room;
          }
        },
        error => {
          this.alertService.error(error);
        });
      this.onCampus = true;
    }
    else if(this.student.local_addr_id){
      this.addressService.getLocalAddress(JSON.parse(sessionStorage.getItem('currentUser')).id).subscribe(
        data => {
          if(data.result != "EOF"){
            this.offCampusAddr = data.result;
          }
        },
        error => {
          this.alertService.error(error);
        }
      );
      this.onCampus = false;
    }

    //get ship to utrucking and east coast location
    if(this.student.east_coast_location_id != 0){
      this.eastCoastLocationService.getByID(JSON.parse(sessionStorage.getItem('currentUser')).id).subscribe(
        data => {
          this.eastCoastLocationName = data.result.name;
        },
        error => {
          this.alertService.error(error);
        }
      );
    }
  }

  checkIfOrdered(): void{
    for (let service of this.potentialServices){
      this.serviceLinkService.getServiceLink({
        service_id: service.service_id,
        user_id: JSON.parse(sessionStorage.getItem('currentUser')).id
      }).subscribe(
        data => {
          if(data.result == "EOF"){
            var availableNumber = 0;
            var currentNumber = 0;
            if((new Date(service.signup_end_date).valueOf()) > (new Date().valueOf()) && service.hidden == 0){
                availableNumber = service.total;
                if(availableNumber != 0){
                    this.serviceLinkService.getCount(service.service_id).subscribe(
                      data => {
                        currentNumber = data[0].count;
                      },
                      error => {
                        this.alertService.error(error);
                      }
                    );
                }
                else{
                  currentNumber = 0;
                }
            if(availableNumber == 0 || availableNumber > currentNumber){
              this.availableServices.push(service);
            }
          }
        }
        else{
          this.orderedServices.push(service);
        }
      },
      error => {
        this.alertService.error(error);
      });
    }
  }


  addService(page, id): void{
    this.serviceType = page;
    this.router.navigate(['/' + this.serviceType], {queryParams: { serviceID: id }});
  }
  timeFromDB(dbTime): string{
    var returnString;
    if((dbTime - 12) > 0){
        returnString = (dbTime - 12) + ":00 PM";
    }
    else if((dbTime - 12) == 0){
      returnString = dbTime + ":00 PM";
    }
    else{
      returnString = dbTime + ":00AM";
    }
    return returnString;
  }
  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }
}
