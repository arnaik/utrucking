import { Component, OnInit } from '@angular/core';
import { Service } from '../models/service.model';
import { Address } from '../models/address.model';
import { Setting } from '../models/setting.model';
import { ServiceService } from '../_services/service.service';
import { UserService } from '../_services/user.service';
import { AlertService } from '../_services/alert.service';
import { ServiceLinkService } from '../_services/service_link.service';
import { MasterScheduleService } from '../_services/master_schedule.service';
import { LivingLocationService } from '../_services/living_location.service';
import { AddressService } from '../_services/address.service';
import { ScheduleService } from '../_services/schedule.service';
import { PeakService } from '../_services/peak.service';
import { SettingService } from '../_services/setting.service';
import { StateService } from '../_services/state.service';
import { CountryService } from '../_services/country.service';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { State } from '../models/state.model';

@Component({
  selector: 'edit_localaddress',
  templateUrl: './edit_localaddress.component.html',
  styleUrls: ['./edit_localaddress.component.css'],
})
export class EditLocalAddressComponent implements OnInit{
  settings: Setting[];
  livingLocations: any;
  student: any = {
    student_id: 0,
    first_name: "",
    last_name: "",
    balance: 0,
    balance_text: "",
    room: 0,
    living_loc_id: 0,
    local_addr_id: 0,
    sched_price: 0
  };
  editLocalBool = false;
  livingLocationID = 0;
  livingLocationName = "Building Name";
  newOffCampusAddr = {
    street: "",
    city: "St. Louis",
    state: "Missouri",
    country: "United States of America",
    province: "",
    zip: "63130"
};
onCampus: any;
onCampusAddr: any = {
  living_location_id: 0,
  name: "",
  room: 0
};
offCampusAddr: Address;
constructor (private serviceService: ServiceService, private router: Router, private userService: UserService, private alertService: AlertService, private serviceLinkService: ServiceLinkService, private masterScheduleService: MasterScheduleService, private addressService: AddressService, private livingLocationService: LivingLocationService, private scheduleService: ScheduleService, private peakService: PeakService, private settingService: SettingService, private countryService: CountryService, private stateService: StateService){}

ngOnInit(): void{
  this.getUserInfo();
  this.getSettings();
  this.livingLocationService.getAll().subscribe(response => this.livingLocations = response);
  //get living location

}
setLivingLocation(livingLocationID: number, livingLocationName: string){
  this.livingLocationID = livingLocationID;
  this.livingLocationName = livingLocationName;
}
getUserInfo(){
  this.userService.getByID(JSON.parse(sessionStorage.getItem('currentUser')).id).subscribe(
    data => {
      this.student = data[0];
      if(this.student.room != 0){
        this.livingLocationService.getLocation(JSON.parse(sessionStorage.getItem('currentUser')).id).subscribe(
          data => {
            if(data.result != "EOF"){
              this.onCampusAddr = data.result;
              this.onCampusAddr.room = this.student.room;
            }
          },
          error => {
            this.alertService.error(error);
          });
        this.onCampus = true;
      }
      else if(this.student.local_addr_id){
        this.addressService.getLocalAddress(JSON.parse(sessionStorage.getItem('currentUser')).id).subscribe(
          data => {
            if(data.result != "EOF"){
              this.offCampusAddr = data.result;
            }
          },
          error => {
            this.alertService.error(error);
          }
        );
        this.onCampus = false;
      }
    },
    error => {
      this.alertService.error(error);
    }
  );
}
editLocalAddress(){
  if(this.onCampus){
    if(this.newOffCampusAddr.street != ""){
      this.addressService.createAddress(this.newOffCampusAddr).subscribe(
        data => {
          console.log("successfully created address");
          console.log(data.insertID);
          this.userService.updateOffCampusInfo({
            livingLocationID: this.livingLocationID,
            localAddressID: data.insertID,
            userID: JSON.parse(sessionStorage.getItem('currentUser')).id
          }).subscribe(
            data => {
              console.log("edited off campus info");
            },
            error => {
              this.alertService.error(error);
            }
          );
        },
        error => {
          this.alertService.error(error);
        }
      );
    }
    else{
      this.userService.updateOnCampusInfo({
      livingLocationID: this.livingLocationID,
      room: this.onCampusAddr.room,
      userID: JSON.parse(sessionStorage.getItem('currentUser')).id
    }).subscribe(
      data => {
        console.log("edited on campus info");
      },
      error => {
        this.alertService.error(error);
      }
    );
    }
  }
  else{
    if(this.onCampusAddr.room != 0 && this.livingLocationID != 1){
      console.log(this.onCampusAddr.room);
      this.userService.updateOnCampusInfo({
        livingLocationID: this.livingLocationID,
        room: this.onCampusAddr.room,
        userID: JSON.parse(sessionStorage.getItem('currentUser')).id
      }).subscribe(
        data => {
          console.log("edited on campus info");
        },
        error => {
          this.alertService.error(error);
        }
      );
      }
    else{
      this.offCampusAddr.street = this.newOffCampusAddr.street;
      this.addressService.updateAddress(this.offCampusAddr).subscribe(
        data => {
          console.log('successfully edited offcampus address in address');
        },
        error => {
          this.alertService.error(error);
        }
      );
    }
  }
  }

timeFromDB(dbTime): string{
  var returnString;
  if((dbTime - 12) > 0){
      returnString = (dbTime - 12) + ":00 PM";
  }
  else if((dbTime - 12) == 0){
    returnString = dbTime + ":00 PM";
  }
  else{
    returnString = dbTime + ":00AM";
  }
  return returnString;
}

getSettings(): void{
  this.settingService.getSettings().subscribe(response => this.settings = response);
}
}
