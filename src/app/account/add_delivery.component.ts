import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
//models
import { Address } from '../models/address.model';
import { State } from '../models/state.model';
import { Schedule } from '../models/schedule.model';
import { User } from '../models/user.model';
import { Service } from '../models/service.model';
//services
import { LivingLocation } from '../models/living_location.model';
import { ServiceService } from '../_services/service.service';
import { AddressService } from '../_services/address.service';
import { ServiceLinkService } from '../_services/service_link.service';
import { LivingLocationService } from '../_services/living_location.service';
import { AlertService } from '../_services/alert.service';
import { UserService } from '../_services/user.service';
import { StateService } from '../_services/state.service';
import { CountryService } from '../_services/country.service';
import { ScheduleService } from '../_services/schedule.service';
import { ServiceLink } from '../models/service_link.model';
import { MasterScheduleService } from '../_services/master_schedule.service';
import { EastCoastLocationService } from '../_services/east_coast_location.service';
import { EmailService } from '../_services/email.service';

@Component({
  selector: 'add_delivery',
  templateUrl: './add_delivery.component.html',
  styleUrls: ['./add_delivery.component.css'],
})
export class AddDeliveryComponent implements OnInit{
  service: any = {};
  serviceID = 0;
  offCampusAddress: any = {
    street: "",
    city: "St. Louis",
    state: "MO",
    country: "United States of America",
    zip: "63130",
    province: ""
  }
  livingLocations: any;
  livingLocationID = 0;
  livingLocationName = "Building Name";
  room: number = 0;
  shippingAddressID = 0;
  schedule: Schedule = {
    schedule_id: 0,
    student_id: JSON.parse(sessionStorage.getItem('currentUser')).id,
    master_schedule_id: 0,
    shipping_addr_id: 0
  };
  serviceLink: any = {};
  eastCoastLocations: any;
  showAll = true;
  student: any;
  mailOptions: any = {};

  constructor (private serviceService: ServiceService, private addressService: AddressService, private route: ActivatedRoute, private router: Router, private serviceLinkService: ServiceLinkService, private livingLocationService: LivingLocationService, private alertService: AlertService, private userService: UserService, private stateService: StateService, private countryService: CountryService, private scheduleService: ScheduleService, private masterScheduleService: MasterScheduleService, private eastCoastLocationService: EastCoastLocationService, private emailService: EmailService){}
  ngOnInit(): void{
    this.route
   .queryParams
   .subscribe(params => {
     // Defaults to 0 if no query param provided.
     this.serviceID = +params['serviceID'] || 0;
   });
   this.getMasterSchedule();
   this.getStudentInfo();
   this.checkIfOrdered();
   this.checkIfOrderedOtherServices();
   this.getServiceInfo();
   this.getLivingLocations();
  }

  getMasterSchedule(): void{
    var nowTimestamp;
    var thenTimestamp;
    this.masterScheduleService.getByID(JSON.parse(sessionStorage.getItem('currentUser')).id).subscribe(
      data => {
        if(data.result == "EOF"){
          nowTimestamp = 1;
          thenTimestamp = 2;
        }else{
          console.log(data);
          let hours = 36;
          nowTimestamp = (new Date).getTime() + (3600 * (2 + hours));
          thenTimestamp = (new Date(data.result.date + " " + data.result.start_time + ":00:00")).getTime();
        }
        if(nowTimestamp > thenTimestamp){
          this.router.navigate(['/profile']);
          this.alertService.success("It is too close to the scheduled date and time for this service to order it");
        }
      },
      error => {
        this.alertService.error(error);
      });
  }

  getStudentInfo(): void{
      this.userService.getByID(JSON.parse(sessionStorage.getItem('currentUser')).id).subscribe(
      data => {
        this.student = data[0];
      },
      error => {
        this.alertService.error(error);
      });
  }

  checkIfOrdered(){
    this.serviceLink.service_id = this.serviceID;
    this.serviceLink.user_id = JSON.parse(sessionStorage.getItem('currentUser')).id;
    this.serviceLinkService.getServiceLink(this.serviceLink).subscribe(
      data => {
        if(data.result == "ALREADY_EXISTS"){
          this.router.navigate(['/profile']);
          this.alertService.success('This service has already been ordered');
        }

      },
      error => {
        this.alertService.error(error);
      }
    );
  }

  checkIfOrderedOtherServices(){
    this.scheduleService.getUserID(JSON.parse(sessionStorage.getItem('currentUser')).id).subscribe(
      data => {
        if(data.result == "EOF"){
          this.showAll = true;
        }
        else{
          this.showAll = false;
        }
      },
      error => {
        this.alertService.error(error);
      }
    );
  }

  getServiceInfo(): void{
    if(this.serviceID){
      this.serviceService.getServiceByID(this.serviceID).subscribe(
        data => {
          this.service = data;
        },
        error => {
          this.alertService.error(error);
        });
    }
  }

  getLivingLocations(): void{
    this.livingLocationService.getAll().subscribe(response => this.livingLocations = response);
  }

  setLivingLocation(livingLocationID: number, livingLocationName: string): void{
    this.livingLocationID = livingLocationID;
    this.livingLocationName = livingLocationName;
  }

  createService(){
    //insert service_link
    this.createServiceLink();
    //on/off campus
    this.updateCampusInfo();
    //add Schedule
    this.updateSchedule();
    //send email
    this.sendEmail();
    //add balance and balance text
    this.updateStudentBalance();
    //move to credit card page
    this.router.navigate(['/credit_card']);
  }

  createServiceLink(): void{
    var params;
    params = {
      serviceID: this.serviceID,
      userID: JSON.parse(sessionStorage.getItem('currentUser')).id
    };
    this.serviceLinkService.addServiceLink(params).subscribe(
      data => {
        console.log("added service link");
      },
      error => {
        this.alertService.error(error);
      }
    );
  }

  updateCampusInfo(): void{
    if(this.livingLocationID == 1){
      this.updateOffCampusInfo();
    }else if(this.livingLocationID > 1){
      this.updateOnCampusInfo();
    }
  }

  updateOffCampusInfo(): void{
    this.addressService.createAddress(this.offCampusAddress).subscribe(
      data => {
        this.userService.updateOffCampusInfo(
          {
            userID: JSON.parse(sessionStorage.getItem('currentUser')).id,
            livingLocationID: this.livingLocationID,
            localAddressID: data.insertID
          }).subscribe(
            data => {
              console.log("updated off campus info successfully");
            },
            error => {
              this.alertService.error(error);
            });
      },
      error => {
        this.alertService.error(error);
      });
  }

  updateOnCampusInfo(){
      this.userService.updateOnCampusInfo(
        {
          userID: JSON.parse(sessionStorage.getItem('currentUser')).id,
          livingLocationID: this.livingLocationID,
          room: this.room
        }
      ).subscribe(
          data => {
            console.log("updated on campus info successfully");
          },
          error => {
            this.alertService.error(error);
          }
      );
  }

  updateSchedule(): void{
    this.scheduleService.getShipID(JSON.parse(sessionStorage.getItem('currentUser')).id).subscribe(
      data => {
        if(data.result == "EOF"){
          this.createNewSchedule(this.schedule);
        }
      },
      error => {
        this.alertService.error(error);
      }
    );
  }
  createNewSchedule(schedule){
    console.log(schedule);
    this.scheduleService.addSchedule(schedule).subscribe(
      data => {
        console.log("added new schedule");
      },
      error => {
        this.alertService.error(error);
      }
    );
  }

  sendEmail(): void{
    //email service
    // this.mailOptions.to = this.student.student_email;
    // this.mailOptions.subject = "Friendly Reminder from UTrucking";
    // this.mailOptions.text = "Dear " + this.student.first_name + " " + this.student.last_name + ", <br><br>" + "You are now signed up for appliance services. To examine your account please visit utrucking.com.<br><br>Thanks!<br>The UTrucking Team"
    // this.emailService.sendEmail(this.mailOptions).subscribe(
    //   data => {
    //     console.log("successfully sent email");
    //   },
    //   error => {
    //     this.alertService.error(error);
    //   }
    // );
  }
  updateStudentBalance(): void{
    if(this.service.price + this.service.deposit > 0){
      console.log(this.service.price + this.service.deposit);
      this.student.balance = this.student.balance + this.service.price + this.service.deposit;
      console.log(this.student.balance);
      if(this.student.balance_text){
        this.student.balance_text = this.student.balance_text + ", " + this.service.name;
        console.log(this.student.balance_text);
      }else{
        this.student.balance_text = this.service.name;
        console.log(this.student.balanceText);
      }
      if(this.student.balance == 0){
        console.log("uh oh");
        this.student.balance_text == "";
      }
    }
    console.log(this.student);
    this.userService.updateBalance(this.student).subscribe(
      data => {
        console.log("updated balance and balance text");
      },
      error => {
        this.alertService.error(error);
      }
    );
  }

}
