import { Component, OnInit } from '@angular/core';
import { Service } from '../models/service.model';
import { Address } from '../models/address.model';
import { Setting } from '../models/setting.model';
import { ServiceService } from '../_services/service.service';
import { UserService } from '../_services/user.service';
import { AlertService } from '../_services/alert.service';
import { ServiceLinkService } from '../_services/service_link.service';
import { MasterScheduleService } from '../_services/master_schedule.service';
import { LivingLocationService } from '../_services/living_location.service';
import { AddressService } from '../_services/address.service';
import { ScheduleService } from '../_services/schedule.service';
import { PeakService } from '../_services/peak.service';
import { SettingService } from '../_services/setting.service';
import { StateService } from '../_services/state.service';
import { CountryService } from '../_services/country.service';
import { EastCoastLocationService } from '../_services/east_coast_location.service';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { State } from '../models/state.model';

@Component({
  selector: 'edit_eastcoastlocation',
  templateUrl: './edit_eastcoastlocation.component.html',
  styleUrls: ['./edit_eastcoastlocation.component.css'],
})
export class EditEastCoastLocationComponent implements OnInit{
  student: any = {
    student_id: 0,
    first_name: "",
    last_name: "",
    balance: 0,
    balance_text: "",
    room: 0,
    living_loc_id: 0,
    local_addr_id: 0,
    sched_price: 0
  };
  eastCoastLocations: any;
  currentEastCoastLocation: any;
  newEastCoastLocation: any = {
    east_coast_location_id: 0,
    name: ""
  }
constructor (private serviceService: ServiceService, private router: Router, private userService: UserService, private alertService: AlertService, private serviceLinkService: ServiceLinkService, private masterScheduleService: MasterScheduleService, private addressService: AddressService, private livingLocationService: LivingLocationService, private scheduleService: ScheduleService, private peakService: PeakService, private settingService: SettingService, private countryService: CountryService, private stateService: StateService, private eastCoastLocationService: EastCoastLocationService){}

ngOnInit(): void{
  this.getUserInfo();
  this.eastCoastLocationService.getAll().subscribe(
    data => {
      this.eastCoastLocations = data;
    },
    error => {
      this.alertService.error(error);
    }
  );
  this.eastCoastLocationService.getByID(JSON.parse(sessionStorage.getItem('currentUser')).id).subscribe(
    data => {
      this.currentEastCoastLocation = data.result;
    },
    error => {
      this.alertService.error(error);
    }
  );
}
getUserInfo(){
  this.userService.getByID(JSON.parse(sessionStorage.getItem('currentUser')).id).subscribe(
    data => {
      this.student = data[0];
    },
    error => {
      this.alertService.error(error);
    }
  );
}
changeEastCoastLocation(){
  this.student.east_coast_location_id = this.newEastCoastLocation.east_coast_location_id;
  this.userService.updatePickup(this.student).subscribe(
    data => {
      console.log("success!");
      this.router.navigate(['/profile']);
    },
    error => {
      this.alertService.error(error);
    }
  );
}

}
