import { Component, OnInit } from '@angular/core';
import { Service } from '../models/service.model';
import { Address } from '../models/address.model';
import { Setting } from '../models/setting.model';
import { ServiceService } from '../_services/service.service';
import { UserService } from '../_services/user.service';
import { AlertService } from '../_services/alert.service';
import { ServiceLinkService } from '../_services/service_link.service';
import { MasterScheduleService } from '../_services/master_schedule.service';
import { LivingLocationService } from '../_services/living_location.service';
import { AddressService } from '../_services/address.service';
import { ScheduleService } from '../_services/schedule.service';
import { PeakService } from '../_services/peak.service';
import { SettingService } from '../_services/setting.service';
import { StateService } from '../_services/state.service';
import { CountryService } from '../_services/country.service';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { State } from '../models/state.model';

@Component({
  selector: 'edit_shippingaddress',
  templateUrl: './edit_shippingaddress.component.html',
  styleUrls: ['./edit_shippingaddress.component.css'],
})
export class EditShippingAddressComponent implements OnInit{
  states: State[] = [];
  countries: String[] = [];
  settings: Setting[];
  student: any = {
    student_id: 0,
    first_name: "",
    last_name: "",
    balance: 0,
    balance_text: "",
    room: 0,
    living_loc_id: 0,
    local_addr_id: 0,
    sched_price: 0
  };
  editShippingBool = false;
  homeAddress: Address;
  homeAddressBool = false;
  shippingAddress: Address;

constructor (private serviceService: ServiceService, private router: Router, private userService: UserService, private alertService: AlertService, private serviceLinkService: ServiceLinkService, private masterScheduleService: MasterScheduleService, private addressService: AddressService, private livingLocationService: LivingLocationService, private scheduleService: ScheduleService, private peakService: PeakService, private settingService: SettingService, private countryService: CountryService, private stateService: StateService){}

ngOnInit(): void{
  this.getUserInfo();
  this.getSettings();
  this.addressService.getHomeAddress(JSON.parse(sessionStorage.getItem('currentUser')).id).subscribe(
    data => {
      this.homeAddress = data;
    },
    error => {
      this.alertService.error(error);
    });
  this.countries = this.countryService.getCountries();
  this.states = this.stateService.getStates();
  this.addressService.getShippingAddress(JSON.parse(sessionStorage.getItem('currentUser')).id).subscribe(
    data => {
      if(data.result != "EOF"){
        this.shippingAddress = data.result;
      }
    },
    error => {
      this.alertService.error(error);
    });
}
getUserInfo(){
  this.userService.getByID(JSON.parse(sessionStorage.getItem('currentUser')).id).subscribe(
    data => {
      this.student = data[0];
    },
    error => {
      this.alertService.error(error);
    }
  );
}

useHomeAddress(){
  this.homeAddressBool = true;
}
addNewAddress(){
  this.homeAddressBool = false;
}
editShippingAddress(){
  var newScheduleAddressID;
  if(this.homeAddressBool){
    if(this.shippingAddress.address_id != this.homeAddress.address_id){
      this.scheduleService.updateSchedule(
        {
        shipping_addr_id: this.homeAddress.address_id,
        student_id: JSON.parse(sessionStorage.getItem('currentUser')).id
        }).subscribe(
        data => {
          console.log("updated shipping address in schedule successfully");
          this.router.navigate(['/profile']);
        },
        error => {
          this.alertService.error(error);
        }
      );
    }
  }else{
    if(this.shippingAddress.address_id == this.homeAddress.address_id){
      this.addressService.createAddress(this.shippingAddress).subscribe(
        data => {
          newScheduleAddressID = data.insertID;
          console.log(newScheduleAddressID);
          console.log(data);
          console.log(data.insertID);
          this.scheduleService.updateSchedule({
            shipping_addr_id: newScheduleAddressID,
            student_id: JSON.parse(sessionStorage.getItem('currentUser')).id
          }).subscribe(
            data => {
              console.log("updated shipping address in schedule");
              this.router.navigate(['/profile']);
            },
            error => {
              this.alertService.error(error);
            }
          );
        },
        error => {
          this.alertService.error(error);
        }
      );
    }else{
      this.addressService.updateAddress(this.shippingAddress).subscribe(
        data => {
          console.log("updated shipping addresss successfully");
          this.router.navigate(['/profile']);
        },
        error => {
          this.alertService.error(error);
        }
      );
    }
  }
}
timeFromDB(dbTime): string{
  var returnString;
  if((dbTime - 12) > 0){
      returnString = (dbTime - 12) + ":00 PM";
  }
  else if((dbTime - 12) == 0){
    returnString = dbTime + ":00 PM";
  }
  else{
    returnString = dbTime + ":00AM";
  }
  return returnString;
}

getSettings(): void{
  this.settingService.getSettings().subscribe(response => this.settings = response);
}
}
