import { Component,Input, OnInit } from '@angular/core';
import { State } from '../models/state.model';
import { StateService } from '../_services/state.service';
import { User } from '../models/user.model';
import { Address } from '../models/address.model';
import { AlertService } from '../_services/alert.service';
import { UserService } from '../_services/user.service';
import { CountryService } from '../_services/country.service';
import { AddressService } from '../_services/address.service';
import { PaymentService } from '../_services/payment.service';
import { AdminMsgService } from '../_services/admin_msg.service';
import { Router } from '@angular/router';

@Component({
  selector: 'credit_card',
  templateUrl: './credit_card.component.html',
  styleUrls: ['./credit_card.component.css']
})
export class CreditCardComponent implements OnInit {
  account: any = {
    first_name: "",
    last_name: "",
    cc_number: "",
    cc_expiration: "",
    cc_code: "",
    cc_type: ""
  };
  states: State[] = [];
  countries: String[] = [];
  finalAddress: any = {
    address_id: 0,
    street: "",
    city: "",
    zip: "",
    state: "",
    country: ""
  }
  homeAddress: any = {
    address_id: 0,
    street: "",
    city: "",
    zip: "",
    state: "",
    country: ""
  };
  billingAddress: any = {
    address_id: 0,
    street: "",
    city: "",
    zip: "",
    state: "",
    country: ""
  };
  params: any = {
    student_id: 0,
    amount: 0,
    note: "",
    first_name: "",
    last_name: "",
    cc_number: "",
    cc_expiration: "",
    cc_code: "",
    cc_type: "",
    street: "",
    city: "",
    zip: "",
    state: "",
    country: ""
  }
  student: any;
  newBilling: any = {
    student_id: 0,
    billing_addr_id: 0
  }
  typeAddress: any;
  adminMsg: any = {
    text: ""
  }
  constructor(private router: Router, private userService: UserService, private stateService: StateService, private alertService: AlertService, private countryService: CountryService,
  private addressService: AddressService, private paymentService: PaymentService, private adminMsgService: AdminMsgService){}
  ngOnInit(): void{
    this.getStates();
    this.getCountries();
    this.getHomeAddress();
    this.getBillingAddress();
    this.getBalance();
  }
  getCountries(): void{
    this.countries = this.countryService.getCountries();
  }
  getStates(): void{
    this.states = this.stateService.getStates();
  }
  getHomeAddress(): void{
    this.addressService.getHomeAddress(JSON.parse(sessionStorage.getItem('currentUser')).id).subscribe(
      data => {
        this.homeAddress = data;
      },
      error => {
        this.alertService.error(error);
      });
  }
  getBillingAddress(): void{
    this.addressService.getBillingAddress(JSON.parse(sessionStorage.getItem('currentUser')).id).subscribe(
      data => {
        if(data.address_id > 0){
          this.billingAddress = data;
          console.log(this.billingAddress);
        }
      },
      error => {
        this.alertService.error(error);
      });
  }
  getBalance(): void{
    this.userService.getByID(JSON.parse(sessionStorage.getItem('currentUser')).id).subscribe(
      data => {
        this.student = data[0];
        console.log(this.student);
      },
      error => {
        this.alertService.error(error);
      });
  }
  setHomeAddress(): void{
    this.typeAddress = "Home";
    this.finalAddress = this.homeAddress;
  }
  setBillingAddress(): void{
    this.typeAddress = "Billing";
    this.finalAddress = this.billingAddress;
  }
  setNewAddress(): void{
    this.typeAddress = "New";
    this.finalAddress = {
      street: "",
      city: "",
      zip: "",
      state: "",
      country: ""
    };
  }
  chargeCard(): void{
    //first try to charge card
    this.params.student_id = JSON.parse(sessionStorage.getItem('currentUser')).id;
    this.params.amount = this.student.balance;
    this.params.note = this.student.balance_text;
    this.params.first_name = this.account.first_name;
    this.params.last_name = this.account.last_name;
    this.params.cc_number = this.account.cc_number;
    this.params.cc_expiration = this.account.cc_expiration;
    this.params.cc_code = this.account.cc_code;
    this.params.cc_type = this.account.cc_type;
    this.params.street = this.finalAddress.street;
    this.params.city = this.finalAddress.city;
    this.params.zip = this.finalAddress.zip;
    this.params.state = this.finalAddress.state;
    this.params.country = this.finalAddress.country;
    this.paymentService.chargeCard(this.params).subscribe(
      data => {
        this.alertService.success("The charge successfully went through!")
        //if successful - edit billing info, address info, balance etc.
        //edit balance
        this.student.balance = 0;
        this.student.balance_text = '';
        this.userService.updateBalance(this.student).subscribe(
          data => {
            console.log("updated balance");
          },
          error => {

            this.alertService.error(error);
          }
        );
        //edit address
        console.log(this.typeAddress);
        console.log(this.billingAddress.street);
        if((this.typeAddress == "Billing") && (this.student.billing_addr_id > 0)){
          this.addressService.updateAddress(this.billingAddress).subscribe(
            data => {
              console.log("edited billing address");
            },
            error => {
              this.alertService.error(error);
            }
          );
        }else if((this.typeAddress == "New") || ((this.typeAddress == "Billing") && (this.student.billing_addr_id == 0))){
          this.addressService.createAddress(this.finalAddress).subscribe(
            data => {
              console.log("added new billing address");
              this.newBilling.student_id = JSON.parse(sessionStorage.getItem('currentUser')).id;
              this.newBilling.billing_addr_id = data.insertID;
              this.userService.updateBilling(this.newBilling).subscribe(
                data => {
                  console.log("updated user billing");
                },
                error => {
                  this.alertService.error(error);
                }
              );
            },
            error => {
              this.alertService.error(error);
            }
          );
        }
        else if(this.typeAddress == "Home"){
          this.newBilling.student_id = JSON.parse(sessionStorage.getItem('currentUser')).id;
          this.newBilling.billing_addr_id = this.homeAddress.address_id;
          this.userService.updateBilling(this.newBilling).subscribe(
            data => {
              console.log("updated user billing");
            },
            error => {
              this.alertService.error(error);
            }
          );
        }
      },
      error => {
        console.log(error);
        this.adminMsg.text = "Charge Failed: $" + this.params.amount + " for " + this.account.first_name + " " + this.account.last_name + " Reason: " + error;
        this.adminMsgService.create(this.adminMsg).subscribe(
          data => {
            console.log("added admin msg");
          },
          error => {
            this.alertService.error(error);
          }
        );
        this.alertService.error(error);
      }
    );
  }
}
