import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { AgmCoreModule } from '@agm/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { NavigationComponent } from './navigation/navigation.component';
import { DashboardComponent } from './navigation/dashboard.component';
import { FallMainComponent } from './fall/fall-main.component';
import { FallServicesComponent } from './fall/fall-services.component';
import { SpringMainComponent } from './spring/spring-main.component';
import { SpringServicesComponent } from './spring/spring-services.component';
import { SpringPricesComponent } from './spring/spring-prices.component';
import { ProfileComponent } from './account/profile.component';
import { FAQComponent } from './main/faqs.component';
import { AboutComponent } from './main/about.component';
import { PricesComponent } from './prices.component';
import { TermsComponent } from './main/terms.component';
import { WinterServicesComponent } from './winter/winter-services.component';
import { RegisterUserComponent } from './main/register.component';
import { LoginComponent } from './main/login.component';
import { SeasonService } from './_services/season.service';
import { StateService } from './_services/state.service';
import { ServiceService } from './_services/service.service';
import { AlertComponent } from './alert.component';
import { AlertService } from './_services/alert.service';
import { UserService } from './_services/user.service';
import { AuthenticationService } from './_services/authentication.service';
import { customHttpProvider } from './_helpers/custom-http';
import { AuthGuard } from './_guard/auth.guard';
import { CountryService } from './_services/country.service';
import { AddPickupComponent } from './account/add_pickup.component';
import { AddressService } from './_services/address.service';
import { ServiceLinkService } from './_services/service_link.service';
import { LivingLocationService } from './_services/living_location.service';
import { ScheduleService } from './_services/schedule.service';
import { MasterScheduleService } from './_services/master_schedule.service';
import { PeakService } from './_services/peak.service';
import { SettingService } from './_services/setting.service';
import { EditScheduleComponent } from './account/edit_schedule.component';
import { EditShippingAddressComponent } from './account/edit_shippingaddress.component';
import { EditLocalAddressComponent } from './account/edit_localaddress.component';
import { EditAccountComponent } from './account/edit_account.component';
import { EditPasswordComponent } from './account/edit_password.component';
import { EditEastCoastLocationComponent } from './account/edit_eastcoastlocation.component';
import { AddShipDeliveryComponent } from './account/add_shipdelivery.component';
import { AddDeliveryComponent } from './account/add_delivery.component';
import { AddApplianceComponent } from './account/add_appliance.component';
import { EastCoastLocationService } from './_services/east_coast_location.service';
import { CreditCardComponent } from './account/credit_card.component';
import { ListPaymentComponent } from './account/list_payment.component';
import { PaymentService } from './_services/payment.service';
import { AdminMsgService } from './_services/admin_msg.service';
import { EmailService } from './_services/email.service';
//import { AdminComponent } from './admin/admin.component';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    DashboardComponent,
    FallMainComponent,
    FallServicesComponent,
    FAQComponent,
    AboutComponent,
    PricesComponent,
    TermsComponent,
    WinterServicesComponent,
    RegisterUserComponent,
    AlertComponent,
    LoginComponent,
    SpringMainComponent,
    SpringServicesComponent,
    SpringPricesComponent,
    ProfileComponent,
    AddPickupComponent,
    EditScheduleComponent,
    EditShippingAddressComponent,
    EditLocalAddressComponent,
    EditAccountComponent,
    EditPasswordComponent,
    AddShipDeliveryComponent,
    AddDeliveryComponent,
    AddApplianceComponent,
    CreditCardComponent,
    EditEastCoastLocationComponent,
    ListPaymentComponent
  ],
  imports: [
    NgbModule.forRoot(),
    HttpModule,
    FormsModule,
    RouterModule.forRoot([
      {
        path: 'home',
        component: SpringMainComponent
      },
      {
        path: 'edit_schedule',
        component: EditScheduleComponent
      },
      {
        path: 'spring-services',
        component: SpringServicesComponent
      },
      {
        path: 'faqs',
        component: FAQComponent
      },
      {
        path: 'about',
        component: AboutComponent
      },
      {
        path: 'prices',
        component: SpringPricesComponent
      },
      {
        path:'terms',
        component: TermsComponent
      },
      {
        path:'winter-services',
        component: WinterServicesComponent
      },
      {
        path:'register',
        component: RegisterUserComponent
      },
      {
        path:'login',
        component: LoginComponent
      },
      {
        path:'profile',
        component: ProfileComponent,
        canActivate: [AuthGuard]
      },
      {
        path:'add_pickup',
        component: AddPickupComponent,
        canActivate: [AuthGuard]
      },
      {
        path:'edit_shippingaddress',
        component: EditShippingAddressComponent,
        canActivate: [AuthGuard]
      },
      {
        path:'edit_localaddress',
        component: EditLocalAddressComponent,
        canActivate: [AuthGuard]
      },
      {
        path:'edit_account',
        component: EditAccountComponent,
        canActivate: [AuthGuard]
      },
      {
        path:'edit_password',
        component: EditPasswordComponent,
        canActivate: [AuthGuard]
      },
      {
        path:'edit_eastcoastlocation',
        component: EditEastCoastLocationComponent,
        canActivate: [AuthGuard]
      },
      {
        path:'add_shipdelivery',
        component: AddShipDeliveryComponent,
        canActivate: [AuthGuard]
      },
      {
        path:'add_delivery',
        component: AddDeliveryComponent,
        canActivate: [AuthGuard]
      },
      {
        path:'add_appliance',
        component: AddApplianceComponent,
        canActivate: [AuthGuard]
      },
      {
        path:'credit_card',
        component: CreditCardComponent,
        canActivate: [AuthGuard]
      },
      {
        path:'list_payment',
        component: ListPaymentComponent,
        canActivate: [AuthGuard]
      },
      {
        path: '',
        redirectTo: 'home',
        pathMatch: "full"
      },
    ]),
    BrowserModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBKiVpB3KoQ9bOgEgoNZbtuzhDgAEXYhz0'
    })
  ],
  providers: [ EmailService, AdminMsgService, PaymentService, SettingService, PeakService, MasterScheduleService, ScheduleService, LivingLocationService, ServiceLinkService, AddressService, SeasonService, UserService, StateService, AlertService, customHttpProvider, AuthGuard, AuthenticationService, CountryService, ServiceService, EastCoastLocationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
