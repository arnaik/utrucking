<%

' UTrucking
' var.asp
' Include file for functions and setup

' Force Secure Connection
If Request.ServerVariables("HTTPS") = "off" AND Request.ServerVariables("URL") <> "/index.asp" AND Request.ServerVariables("URL") <> "/signup.asp" Then
	 Response.Redirect "https://www.utrucking.com" & Request.ServerVariables("URL")
End If

MOVEIN = 0
WINTER = 1
MOVEOUT = 2

' Site Root
Root = "https://www.utrucking.com/"
AbsoluteRoot = "/"
FullyQualifiedRoot = "https://www.utrucking.com/"

' Database Connection Parameters
ConnStr = "Provider=sqloledb;Data Source=sqla15.webcontrolcenter.com;User ID=utrucking;Password=feiler;Initial Catalog=oldsite;"

' Mode For Credit Card Transactions
Mode = "AUTH_CAPTURE"

' Database Table Prefix
Prefix = "New_"

' Function To Format A Date For Database Entry
Function DateToDB(VarDate)
	Temp = Split(VarDate, "/")
	DateToDB = Temp(2) & "-"
	If Len(Temp(0)) = 1 Then
		DateToDB = DateToDB & "0"
	End If
	DateToDB = DateToDB & Temp(0) & "-"
	If Len(Temp(1)) = 1 Then
		DateToDB = DateToDB & "0"
	End If
	DateToDB = DateToDB & Temp(1)
End Function

' Function To Format A Time For Database Entry
Function TimeToDB(VarTime, VarMeridian)
	Temp = Split(VarTime, ":")
	TimeToDB = Temp(0)
	If VarMeridian = "PM" And Temp(0) < 12 Then
		TimeToDB = TimeToDB + 12
	ElseIf VarMeridian = "AM" And Temp(0) = 12 Then
		TimeToDB = 0
	End If
	TimeToDB = TimeToDB + (Temp(1) / 60)
End Function

' Function To Format A Time For Display
Function TimeFromDB(VarTime)
	If InStr(1, "" & VarTime, ".", 1) > 0 Then
		Temp = Split("" & VarTime, ".")
		If Temp(0) > 12 Then
			Temp(0) = Temp(0) - 12
			TimeFromDB = "PM"
		Else
			If Temp(0) = 12 Then
				TimeFromDB = "PM"
			ElseIf Temp(0) = 0 Then
				Temp(0) = 12
				TimeFromDB = "AM"
			Else
				TimeFromDB = "AM"
			End If
		End If
		TempMinutes = Temp(1) * 60
		If Len(TempMinutes) > 2 Then
			Minutes = CInt(Left("" & TempMinutes, 2))
			If CInt(Mid("" & TempMinutes, 3, 1)) >= 5 Then
				Minutes = Minutes + 1
			End If
		Else
			Minutes = TempMinutes
		End If
		TimeFromDB = Temp(0) & ":" & Minutes & " " & TimeFromDB
	Else
		If CInt(VarTime) > 12 Then
			VarTime = CInt(VarTime) - 12
			TimeFromDB = "PM"
		Else
			If CInt(VarTime) = 12 Then
				TimeFromDB = "PM"
			ElseIf CInt(VarTime) = 0 Then
				VarTime = 12
				TimeFromDB = "AM"
			Else
				TimeFromDB = "AM"
			End If
		End If
		TimeFromDB = VarTime & ":00 " & TimeFromDB
	End If
End Function

' Function To Encapsulate UPDATE or INSERT Query And Handle Errors
Function RunQuery(SQL, Conn)
	On Error Resume Next
	Conn.Execute SQL
	If Not Err.Number = 0 Then
		Response.Write "<p>There has been a database error:<br />"
		Response.Write Err.Source & "<br />" & Err.Description & "<br />" & Err.Number
		Response.Write "</p>"
		Response.Write "<p>SQL: " & SQL & "</p>"
		Response.End
	End If
	On Error Goto 0
End Function

' Write Database Value Unless Form Value Is NonNull And Different
Function WriteNewest(DatabaseValue, FormValue)
	If FormValue = "" Then
		WriteNewest = DatabaseValue
	Else
		WriteNewest = FormValue
	End If
End Function

' Write Checked Status Of Checkbox Or Radio Button
Function FillCheckBox(FieldName, TriggerValue)
	If Request.Form("FieldName") = TriggerValue Then
		Response.Write " checked"
	End If
End Function

' Return A Phone Number In A Pretty Way
Function FormatPhone(Digits)
	If Len(Digits) <> 10 Or Not IsNumeric(Digits) Then
		FormatPhone = Digits
	Else
		FormatPhone = "(" & Left(Digits, 3) & ") " & Mid(Digits, 4, 3) & "-" & Right(Digits, 4)
	End If
End Function

' Create a Unix timestamp from a given date
Function UnixTimestamp(VarDateTime)
	UnixTimestamp = DateDiff("s", "01/01/1970 00:00:00", VarDateTime)
End Function

' EMail Checker
Function IsValidEMail(EMail)
	IsValidEMail = True
	If Len(EMail) < 5 Then
		IsValidEMail = False
	Else
		If Instr(1, EMail, " ") <> 0 Or Instr(1, EMail, "'") <> 0 Then
			IsValidEMail = False
		Else
			If InStr(1, EMail, "@", 1) < 2 Then
				IsValidEMail = False
			Else
				If InStrRev(EMail, ".") < InStr(1, EMail, "@", 1) + 2 Then
					IsValidEMail = False
				End If
			End If
		End If
	End If
End Function

' Proper Case from "hello bob" to "Hello Bob"
Function PCase(ByVal strInput)' As String
        Dim I 'As Integer
        Dim CurrentChar, PrevChar 'As Char
        Dim strOutput 'As String

        PrevChar = ""
        strOutput = ""

        For I = 1 To Len(strInput)
            CurrentChar = Mid(strInput, I, 1)

            Select Case PrevChar
                Case "", " ", ".", "-", ",", """", "'"
                    strOutput = strOutput & UCase(CurrentChar)
                Case Else
                    strOutput = strOutput & LCase(CurrentChar)
            End Select

            PrevChar = CurrentChar
        Next 'I

        PCase = strOutput
    End Function

%>
