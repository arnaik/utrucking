import { Component,Input, OnChanges, OnInit } from '@angular/core';
import { UserService } from '../_services/user.service';

@Component({
  selector: 'spring-main',
  templateUrl: './spring-main.component.html',
  styleUrls: ['./spring-main.component.css']
})
export class SpringMainComponent implements OnChanges, OnInit{
  constructor(private userService: UserService){}
  ngOnInit(): void{
  }
  ngOnChanges(): void{}
}
