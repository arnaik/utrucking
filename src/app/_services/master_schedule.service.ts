import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

import { MasterSchedule } from '../models/master_schedule.model';
import 'rxjs/add/operator/map';

@Injectable()
export class MasterScheduleService {
    constructor(private http: Http) { }
    result:any;

    getByID(id) {
      return this.http.get('/master_schedule/get_master/' + id)
            .map((response: Response) => response.json());
    }
    getMax(id) {
      return this.http.get('/master_schedule/get_max/' + id)
            .map((response: Response) => response.json());
    }
    getOptions(params) {
      console.log("in ts controller");
      console.log(params);
      return this.http.get('/master_schedule/get_options/' + params.id + '&' + params.start_date + '&' + params.end_date)
            .map((response: Response) => response.json());
    }
    // getById(_id: string) {
    //     return this.http.get('/users/' + _id).map((response: Response) => response.json());
    // }
    // create(service: Service) {
    //     return this.http.post('/service/register', service);
    // }
    // update(user: User) {
    //     return this.http.put('/users/' + user._id, user);
    // }
    // delete(_id: string) {
    //     return this.http.delete('/users/' + _id);
    // }
}
