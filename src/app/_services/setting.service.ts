import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response, URLSearchParams } from '@angular/http';

import 'rxjs/add/operator/map';

@Injectable()
export class SettingService {
    constructor(private http: Http) { }

    getSettings() {
        return this.http.get('/settings')
              .map((response: Response) => response.json());
    }
    // create(service: Service) {
    //     return this.http.post('/service/register', service);
    // }
    // update(user: User) {
    //     return this.http.put('/users/' + user._id, user);
    // }
    // delete(_id: string) {
    //     return this.http.delete('/users/' + _id);
    // }
}
