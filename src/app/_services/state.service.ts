import { State } from '../models/state.model';
import { STATES, SERVICE_STATES } from '../local/STATES';
import { Injectable } from '@angular/core';

@Injectable()
export class StateService {
  getServiceStates(): State[] {
    return SERVICE_STATES;
  }
  getStates(): State[] {
    return STATES;
  }
  getDescription(name: string): string {
    return this.getServiceStates().find(state => state.name === name).description;
  }

}
