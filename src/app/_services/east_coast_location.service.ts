import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

import { LivingLocation } from '../models/living_location.model';
import 'rxjs/add/operator/map';

@Injectable()
export class EastCoastLocationService {
    constructor(private http: Http) { }
    result:any;
    getAll() {
      return this.http.get('/east_coast_location')
            .map((response: Response) => response.json());
    }
    getByID(id) {
      return this.http.get('/east_coast_location/' + id)
            .map((response: Response) => response.json());
    }
}
