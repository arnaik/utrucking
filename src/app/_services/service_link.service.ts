import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response, URLSearchParams } from '@angular/http';

import { Service } from '../models/service.model';
import 'rxjs/add/operator/map';

@Injectable()
export class ServiceLinkService {
    constructor(private http: Http) { }
    result:any;
    addServiceLink(params) {
      return this.http.post('/service_link/create', params)
            .map((response: Response) => response);
    }
    getServiceLink(serviceLink) {
      return this.http.get('/service_link/get_link/' + serviceLink.service_id + '&' + serviceLink.user_id)
            .map((response: Response) => response.json());
    }
    getCount(id) {
      return this.http.get('/service_link/get_count/' + id)
            .map((response: Response) => response.json());
    }
    // getById(_id: string) {
    //     return this.http.get('/users/' + _id).map((response: Response) => response.json());
    // }
    // create(service: Service) {
    //     return this.http.post('/service/register', service);
    // }
    // update(user: User) {
    //     return this.http.put('/users/' + user._id, user);
    // }
    // delete(_id: string) {
    //     return this.http.delete('/users/' + _id);
    // }
}
