import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

import { LivingLocation } from '../models/living_location.model';
import 'rxjs/add/operator/map';

@Injectable()
export class LivingLocationService {
    constructor(private http: Http) { }
    result:any;
    getAll() {
      return this.http.get('/living_locations')
            .map((response: Response) => response.json());
    }
    getLocation(id) {
      return this.http.get('/living_locations/get_location/' + id)
            .map((response: Response) => response.json());
    }
    // getById(_id: string) {
    //     return this.http.get('/users/' + _id).map((response: Response) => response.json());
    // }
    // create(service: Service) {
    //     return this.http.post('/service/register', service);
    // }
    // update(user: User) {
    //     return this.http.put('/users/' + user._id, user);
    // }
    // delete(_id: string) {
    //     return this.http.delete('/users/' + _id);
    // }
}
