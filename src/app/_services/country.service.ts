import { Injectable } from '@angular/core';
import { COUNTRIES } from '../local/COUNTRIES';

@Injectable()
export class CountryService {
  getCountries(): String[] {
    return COUNTRIES;
  }
}
