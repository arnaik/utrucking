import { Injectable } from '@angular/core';
import { SEASON } from '../local/SEASON';

@Injectable()
export class SeasonService {
  getSeason(): string {
    return SEASON;
  }

}
