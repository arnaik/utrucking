import { Injectable } from '@angular/core';
import { LOCATIONS } from '../local/LOCATIONS';
import { Location } from '../models/location.model';

@Injectable()
export class LocationService {
  getLocations(): Location[] {
    return LOCATIONS;
  }
  filterByState(state: string): Location[] {
    return this.getLocations().filter(location => location.state === state);
  }
}
