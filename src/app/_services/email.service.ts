import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class EmailService {
    constructor(private http: Http) { }

    sendEmail(content) {
      return this.http.post('/email/send', content)
            .map((response: Response) => response.json());
    }
}
