import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

import { Schedule } from '../models/schedule.model';
import 'rxjs/add/operator/map';

@Injectable()
export class ScheduleService {
    constructor(private http: Http) { }
    result:any;
    addSchedule(schedule) {
      return this.http.post('/schedule/create', schedule)
            .map((response: Response) => response);
    }
    updateSchedule(schedule) {
      return this.http.put('/schedule/update', schedule)
            .map((response: Response) => response);
    }
    getShipID(userID) {
      return this.http.get('/schedule/getShipID/' + userID)
            .map((response: Response) => response.json());
    }
    getUserID(userID) {
      return this.http.get('/schedule/getUserID/' + userID)
            .map((response: Response) => response.json());
    }
    getCountPerTimeSlot(id) {
      return this.http.get('/schedule/get_count_per_ts/' + id)
            .map((response: Response) => response.json());
    }
    getCountPerDay(date) {
      return this.http.get('/schedule/get_count_per_day/' + date)
            .map((response: Response) => response.json());
    }
    updateMaster(params) {
      return this.http.put('/schedule/update_master', params)
            .map((response: Response) => response);
    }
    // getById(_id: string) {
    //     return this.http.get('/users/' + _id).map((response: Response) => response.json());
    // }
    // create(service: Service) {
    //     return this.http.post('/service/register', service);
    // }
    // update(user: User) {
    //     return this.http.put('/users/' + user._id, user);
    // }
    // delete(_id: string) {
    //     return this.http.delete('/users/' + _id);
    // }
}
