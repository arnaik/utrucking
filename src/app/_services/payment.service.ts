import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response, URLSearchParams } from '@angular/http';

import 'rxjs/add/operator/map';

@Injectable()
export class PaymentService {
    constructor(private http: Http) { }

    chargeCard(params) {
      console.log("in service.ts");
      return this.http.post('/payment/charge', params)
            .map((response: Response) => response);
    }
    getByID(id) {
      return this.http.get('/payment/' + id)
            .map((response: Response) => response.json());
    }
}
