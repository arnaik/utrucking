import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

import { Address } from '../models/address.model';
import 'rxjs/add/operator/map';

@Injectable()
export class AdminMsgService {
    constructor(private http: Http) { }
    result:any;
    create(adminMsg) {
      return this.http.post('/admin_msg/create', adminMsg)
            .map((response: Response) => response.json());
    }
}
