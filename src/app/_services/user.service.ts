import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

import { User } from '../models/user.model';
import 'rxjs/add/operator/map';

@Injectable()
export class UserService {
    constructor(private http: Http) { }
    result:any;
    getAll() {
      return this.http.get('/users')
            .map((response: Response) => response.json());
    }
    getByID(id: string) {
        return this.http.get('/users/' + id).map((response: Response) => response.json());
    }
    create(user: User) {
        return this.http.post('/users/register', user)
            .map((response: Response) => response);
    }
    updateOffCampusInfo(params) {
        return this.http.put('/users/update_off_campus_info', params)
          .map((response: Response) => response);
    }
    updateOnCampusInfo(params) {
        return this.http.put('/users/update_on_campus_info', params)
          .map((response: Response) => response);
    }
    updateBalance(params) {
        console.log(params);
        return this.http.put('/users/update_balance', params)
          .map((response: Response) => response);
    }
    updateScheduling(params) {
        return this.http.put('/users/update_scheduling', params)
          .map((response: Response) => response);
    }
    update(params) {
        return this.http.put('/users/update', params)
          .map((response: Response) => response);
    }
    updatePassword(params) {
        return this.http.put('/users/update_password', params)
          .map((response: Response) => response);
    }
    updatePickup(params) {
        return this.http.put('/users/update_pickup', params)
          .map((response: Response) => response);
    }
    updateBilling(params) {
        return this.http.put('/users/update_billing', params)
          .map((response: Response) => response);
    }
    // delete(_id: string) {
    //     return this.http.delete('/users/' + _id);
    // }
}
