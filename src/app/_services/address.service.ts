import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

import { Address } from '../models/address.model';
import 'rxjs/add/operator/map';

@Injectable()
export class AddressService {
    constructor(private http: Http) { }
    result:any;
    createAddress(address) {
      return this.http.post('/addresses/create', address)
            .map((response: Response) => response.json());
    }
    getHomeAddress(id) {
      return this.http.get('/addresses/get_home_address/' + id)
            .map((response: Response) => response.json());
    }
    getBillingAddress(id) {
      return this.http.get('/addresses/get_billing_address/' + id)
            .map((response: Response) => response.json());
    }
    getShippingAddress(id){
      return this.http.get('/addresses/get_shipping_address/' + id)
            .map((response: Response) => response.json());
    }
    getLocalAddress(id){
      return this.http.get('/addresses/get_local_address/' + id)
            .map((response: Response) => response.json());
    }
    updateAddress(address) {
        return this.http.put('/addresses/update_address', address);
    }
    // getById(_id: string) {
    //     return this.http.get('/users/' + _id).map((response: Response) => response.json());
    // }
    // create(service: Service) {
    //     return this.http.post('/service/register', service);
    // }

    // delete(_id: string) {
    //     return this.http.delete('/users/' + _id);
    // }
}
