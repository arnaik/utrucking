import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

import { Service } from '../models/service.model';
import 'rxjs/add/operator/map';

@Injectable()
export class ServiceService {
    constructor(private http: Http) { }
    result:any;
    getAvailable() {
      return this.http.get('/services/get_available')
            .map((response: Response) => response.json());
    }
    getServiceByID(id) {
      return this.http.get('/services/get_service/' + id)
            .map((response: Response) => response.json());
    }
    getServiceDates(id) {
      return this.http.get('/services/get_service_dates/' + id)
            .map((response: Response) => response.json());
    }
    addService(service) {
      return this.http.post('/services/register', service)
            .map((response: Response) => response);
    }
    // getById(_id: string) {
    //     return this.http.get('/users/' + _id).map((response: Response) => response.json());
    // }
    // create(service: Service) {
    //     return this.http.post('/service/register', service);
    // }
    // update(user: User) {
    //     return this.http.put('/users/' + user._id, user);
    // }
    // delete(_id: string) {
    //     return this.http.delete('/users/' + _id);
    // }
}
