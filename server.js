const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const http = require('http');
const app = express();
const expressJwt = require('express-jwt');
const cors = require('cors');
const config = require('./server/config.json');



// API file for interacting with MongoDB
const userApi = require('./server/controllers/user.controller');
const serviceApi = require('./server/controllers/service.controller');
const addressApi = require('./server/controllers/address.controller');
const serviceLinkApi = require('./server/controllers/service_link.controller');
const livingLocationApi = require('./server/controllers/living_location.controller');
const scheduleApi = require('./server/controllers/schedule.controller');
const masterScheduleApi = require('./server/controllers/master_schedule.controller');
const peakApi = require('./server/controllers/peak.controller');
const settingApi = require('./server/controllers/setting.controller');
const eastCoastLocationApi = require('./server/controllers/east_coast_location.controller');
const paymentApi = require('./server/controllers/payment.controller');
const AdminMsgApi = require('./server/controllers/admin_msg.controller');
const emailApi = require('./server/controllers/email.controller');

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Angular DIST output folder
app.use(express.static(path.join('./', 'dist')));

// API location
app.use('/users', userApi);
app.use('/services', serviceApi);
app.use('/addresses', addressApi);
app.use('/service_link', serviceLinkApi);
app.use('/living_locations', livingLocationApi);
app.use('/schedule', scheduleApi);
app.use('/master_schedule', masterScheduleApi);
app.use('/peak', peakApi);
app.use('/settings', settingApi);
app.use('/east_coast_location', eastCoastLocationApi);
app.use('/payment', paymentApi);
app.use('/admin_msg', AdminMsgApi);
app.use('/email', emailApi);

// Send all other requests to the Angular app
app.get('*', (req, res) => {
    res.sendFile(path.resolve('./dist', 'index.html'));
});
app.use(function(req, res, next) {
    if((!req.secure) && (req.get('X-Forwarded-Proto') !== 'https')) {
        res.redirect('https://' + req.get('Host') + req.url);
    }
    else
        next();
});

//Set Port
const port = process.env.PORT || 3000;
app.set('port', port);

const server = http.createServer(app);

server.listen(port, () => console.log(`Server listening on Port ${port}`));
