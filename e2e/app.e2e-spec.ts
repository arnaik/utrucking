import { UtruckingPage } from './app.po';

describe('utrucking App', () => {
  let page: UtruckingPage;

  beforeEach(() => {
    page = new UtruckingPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
